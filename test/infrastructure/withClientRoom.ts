import { GameRoom } from "../../src/client/GameRoom.ts";
import { RoomDatabase } from "../../src/server/RoomDatabase.ts";
import { ServerConnection } from "../../src/server/ServerConnection.ts";
import { DoubleEndedWebSocket } from "../mocks/DoubleEndedWebSocket.ts";
import { silentConsole } from "../mocks/silentConsole.ts";

interface GameRoomData<TMessagePayload> {
  messagePayload: TMessagePayload;
  playerPublicData: unknown;
  roomPublicData: unknown;
  roomPublicLog: unknown;
}

export async function withClientRoom<TMessagePayload>(
  fn: (resources: {
    clientRoom: GameRoom<GameRoomData<TMessagePayload>>;
  }) => Promise<void>
): Promise<void> {
  return await withClientRooms<TMessagePayload>(1, ({ clientRooms }) =>
    fn({ clientRoom: clientRooms[0] })
  );
}

export async function withClientRooms<TMessagePayload>(
  count: number,
  fn: (resources: {
    clientRooms: GameRoom<GameRoomData<TMessagePayload>>[];
  }) => Promise<void>
): Promise<void> {
  const database = new RoomDatabase();
  const sockets = [...Array(count)].map(() => new DoubleEndedWebSocket());
  const serverPromises = sockets.map((socket) =>
    ServerConnection.connect({
      webSocket: socket.server,
      console: silentConsole,
      database,
    })
  );
  const clientRooms = sockets.map(
    (socket) =>
      new GameRoom<GameRoomData<TMessagePayload>>({
        webSocket: () => socket.client,
        timeout: 500,
      })
  );

  try {
    await fn({ clientRooms });
  } catch (e) {
    console.error("Exception thrown. Messages", {
      ...sockets.map((x) => x.allMessages),
    });
    throw e;
  }

  clientRooms.forEach((x) => x.dispose());
  database.dispose();
  await Promise.all(serverPromises);
}

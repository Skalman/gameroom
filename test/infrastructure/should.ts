import {
  assert,
  assertEquals,
  assertNotEquals,
} from "https://deno.land/std@0.90.0/testing/asserts.ts";

export function _<T>(actual: T) {
  return new Should(actual);
}

function returnTrue() {
  return true;
}

class Should<T> {
  constructor(readonly actual: T) {}

  shouldBe(expected: T, msg?: string) {
    assertEquals(this.actual, expected, msg);
  }

  shouldNotBe(expected: T, msg?: string) {
    assertNotEquals(this.actual, expected, msg);
  }

  shouldBeUndefined(msg?: string) {
    assertEquals(this.actual, undefined, msg);
  }

  shouldNotBeNullish(msg?: string) {
    assertNotEquals(this.actual, undefined, msg);
    assertNotEquals(this.actual, null, msg);
  }

  shouldBeOneOf(...expected: T[]) {
    assert(
      expected.includes(this.actual),
      `Actual: ${JSON.stringify(
        this.actual
      )}\nExpected: One of ${JSON.stringify(expected)}`
    );
  }

  shouldBeTypeof(
    expected:
      | "string"
      | "number"
      | "bigint"
      | "boolean"
      | "symbol"
      | "undefined"
      | "object"
      | "function",
    msg?: string
  ) {
    assertEquals(typeof this.actual, expected, msg);
  }

  // deno-lint-ignore no-explicit-any
  shouldBeInstanceof(expected: { new (...args: any[]): T }, msg?: string) {
    if (!(this.actual instanceof expected)) {
      const actualConstructor = (this.actual as Record<string, unknown>)
        ?.constructor?.name;
      const expectedConstructor = expected.name;
      const msgSuffix = msg ? `\n${msg}` : "";

      if (!actualConstructor) {
        assert(
          false,
          `Expected type ${expectedConstructor} but got ${JSON.stringify(
            this.actual
          )}${msgSuffix}`
        );
      } else if (actualConstructor !== expectedConstructor) {
        assertEquals(actualConstructor, expectedConstructor, msg);
      } else {
        assert(
          false,
          `Expected type ${expectedConstructor} but got an instance of another same-named type${msgSuffix}`
        );
      }
    }
    assert(this.actual instanceof expected);
  }

  shouldBeTrue(this: { actual: boolean | undefined }, msg?: string) {
    assertEquals(this.actual, true, msg);
  }

  shouldBeFalse(this: { actual: boolean | undefined }, msg?: string) {
    assertEquals(this.actual, false, msg);
  }

  shouldContain(this: { actual: string }, expected: string, msg?: string) {
    if (!this.actual.includes(expected)) {
      assert(
        false,
        msg ??
          `Expected ${JSON.stringify(this.actual)} to contain ${JSON.stringify(
            expected
          )}`
      );
    }
  }

  //   shouldBeEmpty<TItem>(
  //     this: { actual: TItem[] },
  //     callback?: (item: TItem, index: number, array: TItem[]) => unknown,
  //     msg?: string
  //   ) {
  //     const actual = this.actual.filter(callback ?? returnTrue);
  //     assertEquals(
  //       actual.length,
  //       0,
  //       msg ?? "Expected array not to contain any matching items"
  //     );
  //   }

  shouldContainSingle<TItem>(
    this: { actual: TItem[] },
    callback?: (item: TItem, index: number, array: TItem[]) => unknown,
    msg?: string
  ) {
    const actual = this.actual.filter(callback ?? returnTrue);
    assertEquals(
      actual.length,
      1,
      msg ?? "Expected array to contain a single matching item"
    );
  }

  async shouldThrowAsync<TError>(
    this: { actual: () => Promise<unknown> },
    msg?: string
  ): Promise<TError> {
    try {
      await this.actual();
    } catch (e) {
      return e;
    }

    assert(false, msg ?? "Expected function to throw an exception");
  }
}

import {
  WebSocket as ServerWebSocket,
  WebSocketCloseEvent,
  WebSocketEvent,
  WebSocketMessage,
} from "https://deno.land/std@0.90.0/ws/mod.ts";

interface Pipe<T> extends AsyncIterableIterator<T> {
  push(item: T): void;
  close(): void;
}

function pipe<T>(): Pipe<T> {
  const sentinel = Symbol();
  type TItem = T | typeof sentinel;
  const stack: TItem[] = [];
  let next: ((item: TItem) => void) | undefined;

  async function* iterate(): AsyncIterableIterator<T> {
    while (true) {
      let value: TItem;
      if (stack.length) {
        value = stack.shift()!;
      } else {
        value = await new Promise<TItem>((resolve) => {
          next = resolve;
        });
      }

      if (value === sentinel) {
        return;
      }

      yield value;
    }
  }

  function push(item: TItem) {
    if (next) {
      next(item);
      next = undefined;
    } else {
      stack.push(item);
    }
  }

  return Object.assign(iterate(), {
    push,
    close() {
      push(sentinel);
    },
  });
}

export class DoubleEndedWebSocket {
  readonly server: MockServerWebSocket;
  readonly client: MockClientWebSocket;
  readonly allMessages: {
    to: "server" | "client";
    msg: WebSocketMessage;
  }[] = [];
  readonly messagesToServer: string[] = [];
  readonly messagesToClient: WebSocketMessage[] = [];

  constructor() {
    const pipeToServer = pipe<WebSocketMessage>();
    const pipeToClient = pipe<WebSocketMessage>();

    this.spy(pipeToServer, (x) => {
      if (typeof x === "string") {
        this.messagesToServer.push(x);
        this.allMessages.push({ to: "server", msg: x });
      }
    });
    this.spy(pipeToClient, (x) => {
      this.messagesToClient.push(x);
      this.allMessages.push({ to: "client", msg: x });
    });

    this.server = new MockServerWebSocket(pipeToServer, pipeToClient);
    this.client = new MockClientWebSocket(pipeToClient, pipeToServer);
  }

  close() {
    this.client.close();
    this.server.close();
  }

  private spy<T>(pipe: Pipe<T>, callback: (item: T) => void) {
    const origPush = pipe.push;
    pipe.push = (item) => {
      callback(item);
      origPush(item);
    };
  }
}

class MockClientWebSocket implements globalThis.WebSocket {
  private isClosed = false;
  private readonly listeners = {
    close: new Set([(x: CloseEvent) => this.onclose?.(x)]),
    error: new Set([(x: Event | ErrorEvent) => this.onerror?.(x)]),
    message: new Set([(x: MessageEvent<unknown>) => this.onmessage?.(x)]),
    open: new Set([(x: Event) => this.onopen?.(x)]),
  };

  constructor(
    private readonly msgToSelf: Pipe<WebSocketMessage>,
    private readonly msgToOther: Pipe<WebSocketEvent>
  ) {
    this.listen();
    this.readyState = WebSocket.OPEN;
  }

  send(data: WebSocketMessage): void {
    if (this.isClosed) {
      throw new Error("Cannot send data, because the socket is closed");
    }

    this.msgToOther.push(data);
  }
  close(): void {
    if (this.isClosed) return;
    this.isClosed = true;
    this.readyState = WebSocket.CLOSED;
    this.dispatchEvent(new CloseEvent("close"));
    this.msgToOther.close();
  }

  private async listen() {
    for await (const msg of this.msgToSelf) {
      if (this.isClosed) return;
      this.dispatchEvent(new MessageEvent("message", { data: msg }));
    }

    this.close();
  }

  binaryType!: BinaryType;
  bufferedAmount!: number;
  extensions!: string;
  onclose: ((this: WebSocket, ev: CloseEvent) => unknown) | null = null;
  onerror: ((this: WebSocket, ev: Event | ErrorEvent) => unknown) | null = null;
  onmessage:
    | ((this: WebSocket, ev: MessageEvent<unknown>) => unknown)
    | null = null;
  onopen: ((this: WebSocket, ev: Event) => unknown) | null = null;
  protocol!: string;
  readyState: number;
  url!: string;
  CLOSED!: number;
  CLOSING!: number;
  CONNECTING!: number;
  OPEN!: number;
  addEventListener<K extends keyof WebSocketEventMap>(
    type: K,
    listener: (this: WebSocket, ev: WebSocketEventMap[K]) => void
  ) {
    this.listeners[type].add(listener as () => void);
  }
  removeEventListener<K extends keyof WebSocketEventMap>(
    type: K,
    listener: (this: WebSocket, ev: WebSocketEventMap[K]) => void
  ) {
    this.listeners[type].delete(listener as () => void);
  }
  dispatchEvent(ev: Event) {
    for (const listener of this.listeners[ev.type as keyof WebSocketEventMap]) {
      (listener as (ev: Event) => void).call(this, ev);
    }

    return true;
  }
  [Symbol.toStringTag]: string;
}

class MockServerWebSocket implements ServerWebSocket {
  isClosed = false;

  constructor(
    private readonly msgToSelf: Pipe<WebSocketEvent>,
    private readonly msgToOther: Pipe<WebSocketMessage> // private readonly closeOther: () => void
  ) {}

  async *[Symbol.asyncIterator](): AsyncIterableIterator<WebSocketEvent> {
    for await (const msg of this.msgToSelf) {
      yield msg;
    }

    this.close();
  }

  send(data: WebSocketMessage): Promise<void> {
    if (this.isClosed) Promise.resolve();
    this.msgToOther.push(data);
    return Promise.resolve();
  }

  close() {
    if (!this.isClosed) {
      this.isClosed = true;
      const msg: WebSocketCloseEvent = { code: 0 };
      this.msgToSelf.push(msg);
      this.msgToSelf.close();
      this.msgToOther.close();
    }
    return Promise.resolve();
  }

  get conn(): Deno.Conn {
    throw new Error("Not implemented.");
  }
  ping() {
    return Promise.resolve();
  }
  closeForce = this.close;
}

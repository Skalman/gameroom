export const silentConsole = (Object.fromEntries(
  Object.keys(console).map((x) => [x, () => {}])
) as unknown) as Console;

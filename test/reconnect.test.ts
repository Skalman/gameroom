import { delay } from "https://deno.land/std@0.90.0/async/mod.ts";
import { GameRoom } from "../src/client/GameRoom.ts";
import { RoomDatabase } from "../src/server/RoomDatabase.ts";
import { ServerConnection } from "../src/server/ServerConnection.ts";
import { _ } from "./infrastructure/should.ts";
import { DoubleEndedWebSocket } from "./mocks/DoubleEndedWebSocket.ts";
import { silentConsole } from "./mocks/silentConsole.ts";

class WebSocketFactory {
  currentSocket?: DoubleEndedWebSocket;
  allSockets: DoubleEndedWebSocket[] = [];
  serverConnections: Promise<void>[] = [];

  get createdCount() {
    return this.allSockets.length;
  }

  private database = new RoomDatabase();

  factory = () => {
    this.currentSocket = new DoubleEndedWebSocket();
    const serverConnection = ServerConnection.connect({
      webSocket: this.currentSocket.server,
      database: this.database,
      console: silentConsole,
    });

    this.allSockets.push(this.currentSocket);
    this.serverConnections.push(serverConnection);

    return this.currentSocket.client;
  };

  dispose() {
    this.database.dispose();
  }
}

Deno.test("reconnect to socket", async () => {
  const factory = new WebSocketFactory();

  const clientRoom = new GameRoom({
    webSocket: factory.factory,
    timeout: 10,
    reconnectPolicy: {
      initialBackoffInMilliseconds: 0,
    },
  });

  _(factory.createdCount).shouldBe(1);
  _(clientRoom.webSocketStatus).shouldBeOneOf("connecting", "open");

  await clientRoom.getNextStatus("open");

  _(factory.currentSocket).shouldNotBeNullish();
  factory.currentSocket!.close();
  await clientRoom.getNextStatus("waitingForReconnect");
  _(factory.createdCount).shouldBe(1);
  await clientRoom.getNextStatus("connecting");
  _(factory.createdCount).shouldBe(2);
  await clientRoom.getNextStatus("open");

  clientRoom.dispose();

  await Promise.all(factory.serverConnections);

  factory.dispose();
});

Deno.test("reconnect to room", async () => {
  const factory = new WebSocketFactory();

  const clientRoom = new GameRoom({
    webSocket: factory.factory,
    timeout: 10,
    reconnectPolicy: {
      initialBackoffInMilliseconds: 0,
    },
  });

  await clientRoom.create().getNextJoinInfo();
  factory.currentSocket?.close();
  await clientRoom.getNextStatus("open");
  _(factory.createdCount).shouldBe(2);
  await delay(0);

  const [socketA, socketB] = factory.allSockets;
  const firstMessageA = socketA.messagesToClient[0];
  const firstMessageB = socketB.messagesToClient[0];

  _(firstMessageA).shouldNotBeNullish();
  _(firstMessageA as string).shouldContain('"type":"joinInfo"');
  _(firstMessageA).shouldBe(firstMessageB);

  clientRoom.dispose();

  await Promise.all(factory.serverConnections);

  factory.dispose();
});

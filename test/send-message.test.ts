import { _ } from "./infrastructure/should.ts";
import {
  withClientRoom,
  withClientRooms,
} from "./infrastructure/withClientRoom.ts";

type Messages = "message-1" | { "message-2": true };

Deno.test("send message", () =>
  withClientRooms<Messages>(2, async ({ clientRooms }) => {
    const [a, b] = clientRooms;

    const joinInfo = await a.create().getNextJoinInfo();
    await b.join(joinInfo.roomId).getNextJoinInfo();

    a.sendMessage(1, "message-1");
    const message = await b.getNextMessage();
    _(message).shouldBe({ sender: 0, payload: "message-1" });
  })
);

Deno.test("send message to non-existent player", () =>
  withClientRoom<Messages>(async ({ clientRoom }) => {
    await clientRoom.create().getNextJoinInfo();
    clientRoom.sendMessage(1, { "message-2": true });

    const error = await clientRoom.getNextErrorMessage();
    _(error.reason).shouldBe("playerDoesntExist");
  })
);

Deno.test("send message when other player is offline", () =>
  withClientRooms<Messages>(2, async ({ clientRooms }) => {
    const [a, b] = clientRooms;

    const joinInfoA = await a.create().getNextJoinInfo();
    await b.join(joinInfoA.roomId).getNextJoinInfo();
    await a.leave().getNextByeMessage();

    b.sendMessage(0, "message-1");
    await _(() => a.getNextMessage(undefined, 10)).shouldThrowAsync();
  })
);

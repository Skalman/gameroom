import { GameRoom } from "../src/client/GameRoom.ts";
import { RoomDatabase } from "../src/server/RoomDatabase.ts";
import { ServerConnection } from "../src/server/ServerConnection.ts";
import { _ } from "./infrastructure/should.ts";
import {
  withClientRoom,
  withClientRooms,
} from "./infrastructure/withClientRoom.ts";
import { DoubleEndedWebSocket } from "./mocks/DoubleEndedWebSocket.ts";
import { silentConsole } from "./mocks/silentConsole.ts";

Deno.test("open", async () => {
  const database = new RoomDatabase();
  const socket = new DoubleEndedWebSocket();
  const serverConnection = ServerConnection.connect({
    webSocket: socket.server,
    database,
    console: silentConsole,
  });
  const clientRoom = new GameRoom({ webSocket: () => socket.client });
  socket.close();

  clientRoom.dispose();
  await serverConnection;

  _(socket.allMessages).shouldBe([]);
});

Deno.test("create room", () =>
  withClientRoom(async ({ clientRoom }) => {
    const info = await clientRoom.create().getNextJoinInfo();
    _(info.roomId).shouldBeTypeof("number");
    _(info.slug).shouldBeTypeof("string");
    _(info.password).shouldBeTypeof("string");
    _(info.playerId).shouldBe(0);
    _(info.playerName).shouldBe(undefined);
    _(info.publicData).shouldBe(undefined);

    _(clientRoom.connection.roomId).shouldBe(info.roomId);
    _(clientRoom.connection.slug).shouldBe(info.slug);
    _(clientRoom.connection.password).shouldBe(info.password);
    _(clientRoom.myInfo?.playerId).shouldBe(info.playerId);
    _(clientRoom.myInfo?.playerName).shouldBe(info.playerName);
    _(clientRoom.myInfo?.publicData).shouldBe(info.publicData);
    _(clientRoom.isInRoom).shouldBeTrue();
  })
);

Deno.test("leave room", () =>
  withClientRoom(async ({ clientRoom }) => {
    await clientRoom.create().leave().getNextByeMessage();
    _(clientRoom.isInRoom).shouldBeFalse();
  })
);

Deno.test("create two rooms", () =>
  withClientRoom(async ({ clientRoom }) => {
    const firstJoinInfo = await clientRoom.create().getNextJoinInfo();
    const secondJoinInfo = await clientRoom.create().getNextJoinInfo();

    _(firstJoinInfo.roomId).shouldNotBe(secondJoinInfo.roomId);
    _(firstJoinInfo.slug).shouldNotBe(secondJoinInfo.slug);
  })
);

Deno.test("join room", () =>
  withClientRooms(2, async ({ clientRooms }) => {
    const [a, b] = clientRooms;

    const joinInfoA = await a.create().getNextJoinInfo();
    const joinInfoB = await b.join(joinInfoA.roomId).getNextJoinInfo();

    _(joinInfoB.roomId).shouldBe(joinInfoA.roomId);
    _(joinInfoB.slug).shouldBe(joinInfoA.slug);
    _(joinInfoB.playerId).shouldBe(1);
    _(joinInfoB.password).shouldNotBe(joinInfoA.password);
  })
);

Deno.test("rejoin room", () =>
  withClientRoom(async ({ clientRoom }) => {
    const joinInfo = await clientRoom.create().getNextJoinInfo();
    await clientRoom.leave().getNextByeMessage();

    const secondJoinInfo = await clientRoom.rejoin().getNextJoinInfo();

    _(secondJoinInfo.roomId).shouldBe(joinInfo.roomId);
    _(secondJoinInfo.slug).shouldBe(joinInfo.slug);
    _(secondJoinInfo.playerId).shouldBe(0);
    _(secondJoinInfo.password).shouldBe(joinInfo.password);
  })
);

# Game Room

Game Room aims to be a platform on which you can build games where players.

It consists of two parts: a server and a client library.

Features:

- Create rooms, connect and reconnect to rooms
- Send messages to others in the same room, either to all or only one player
- Set one's nickname, store own public data
- Store public data as a room log
- Store public data about a room
- Reconnect automatically to the WebSocket

Roadmap:

- Clean up unused rooms
- Store the rooms in a database

## Server

The server is written for use with Deno. It is designed to be minimal, only providing features necessary to connect players.

## Client library

The client library allows for easy usage of the server, making it easy to listen for events.

If the connection is broken, the client library will automatically reconnect, using an exponential backoff.

## Examples

[`src/client/example/`](./src/client/example/) contains a few examples applications using the client library.

#!/usr/bin/env sh

set -e

if [ "$1" = '--no-check' ]; then
    CHECK=--no-check
else
    CHECK=
fi

echo 'Run server and integration tests'
deno test $CHECK src/server/ test/

echo 'Run client tests'
deno test $CHECK --config=src/client/tsconfig.json src/client/

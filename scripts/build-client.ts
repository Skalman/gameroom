#!/usr/bin/env -S deno run --unstable --allow-read=src/ --allow-write=dist/ --no-check

export {};

// TODO When `bundle` and `declaration` are supported together, use that instead of this hack.
const license = `
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
`.trim();

const concatenated = [
  license,
  Deno.readTextFileSync("src/shared/messages.ts"),
  Deno.readTextFileSync("src/client/ListenerCollection.ts"),
  Deno.readTextFileSync("src/client/PermanentWebSocket.ts"),
  Deno.readTextFileSync("src/client/GameRoom.ts"),
  Deno.readTextFileSync("src/client/getReactHelpers.ts"),
]
  .join("\n")
  .replace(/\bimport \{[^}]+\} from "[^"]+";\n/g, "");

const result = await Deno.emit("file:///client.ts", {
  sources: { "file:///client.ts": concatenated },
  compilerOptions: {
    lib: ["DOM", "ES2020"],
    module: "esnext",
    strict: true,
    noUnusedLocals: true,
    noUnusedParameters: true,
    declaration: true,
    sourceMap: false,
    target: "es2018",
  },
});

if (result.diagnostics.length) {
  for (const x of result.diagnostics) {
    console.error(`ts(${x.code}): ${x.messageText}`);
    console.error(`  ${x.sourceLine}`);
    console.error();
  }

  Deno.exit(1);
}

Deno.mkdirSync(`dist/client/`, { recursive: true });

const filesAndCode = [
  ...Object.entries(result.files),
  [
    "dist/client/package.json",
    JSON.stringify({
      name: "@game-room/client",
      version: "0.1.3",
      license: "MPL-2.0",
      author: "Dan Wolff",
      description: "A client library for Game Room",
      keywords: ["game room", "websocket"],
      type: "module",
      main: "./lib.js",
      exports: { import: "./lib.js" },
      types: "./lib.d.ts",
      repository: "gitlab:Skalman/gameroom",
    }),
  ] as const,
];

for (let [file, code] of filesAndCode) {
  file = file.replace("file:///client.ts", "dist/client/lib");
  Deno.writeTextFileSync(file, code);
  console.log(`${code.length} bytes  ${file}`);
}

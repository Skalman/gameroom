#!/usr/bin/env -S deno run --unstable --allow-read=src/ --allow-write=dist/ --no-check

export {};

const result = await Deno.emit(`src/server/cli.ts`, {
  bundle: "esm",
});

const bundle = result.files["deno:///bundle.js"];
const hashbang = [
  "#!/usr/bin/env -S",
  "deno",
  "run",
  "--allow-net=localhost:80",
  "--allow-read=src/client/example",
  "-",
  "--host=localhost:80",
  "--public-path=src/client/example",
  "--websocket-path=/ws",
];

const license = `
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
`;

const code = [
  hashbang.join(" "),
  license,
  // Poor man's minification
  bundle.replace(/\n +/g, "\n"),
].join("\n");

Deno.mkdirSync(`dist/`, { recursive: true });
Deno.writeTextFileSync(`dist/server.js`, code);

console.log(`Wrote ${code.length} bytes`);

#!/usr/bin/env -S deno run --allow-run --unstable --no-check

export {};

const commands = {
  bundle: [
    "deno",
    "bundle",
    "--unstable",
    "--watch",
    "--no-check",
    "--config=src/client/tsconfig.json",
    "src/client/example/main.ts",
    "src/client/example/main.js",
  ],

  run: [
    "deno",
    "run",
    "--no-check",
    "--allow-net=localhost:8080",
    "--allow-read=src/client/example,dist/room.sqlite,dist/room.sqlite-journal",
    "--allow-write=dist/room.sqlite,dist/room.sqlite-journal",
    "src/server/cli.ts",
    "--host=localhost:8080",
    "--public-path=src/client/example",
    "--database=dist/room.sqlite",
    "--websocket-path=/ws",
  ],
};

console.log(Deno.noColor ? "Run commands" : "\x1b[32mRun commands\x1b[39m");
Object.values(commands).forEach((x) => console.log(x.join(" ")));

const processes = Object.values(commands).map((cmd) =>
  Deno.run({
    cmd,
    stdout: "inherit",
    stderr: "inherit",
  })
);

const result = await Promise.any(processes.map((x) => x.status()));

for (const process of processes) {
  process.close();
  process.kill(9);
}

Deno.exit(result.code);

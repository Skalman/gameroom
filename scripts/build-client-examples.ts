#!/usr/bin/env -S deno run --unstable --allow-run --allow-read=src/ --allow-write=dist/ --no-check

import { parseArgs } from "../src/shared/parseArgs.ts";

const status = await Deno.run({
  cmd: ["cp", "-r", "src/client/example/", "dist/example"],
  stdout: "inherit",
  stderr: "inherit",
}).status();

if (!status.success) {
  Deno.exit(status.code);
}

const result = await Deno.emit(`src/client/example/main.ts`, {
  bundle: "esm",
});

const bundle = result.files["deno:///bundle.js"];

const args = parseArgs(Deno.args);

const license = `
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
`;

let webSocketUrlDeclaration: string | undefined;
if (args["websocket-url"]) {
  webSocketUrlDeclaration = `const gameRoomWebSocketUrl = ${JSON.stringify(
    args["websocket-url"]
  )};`;
} else {
  console.log(
    "Using default WebSocket URL. You can specify which URL to use with --websocket-url=wss://example.com/ws"
  );
}

const code = [
  license,
  webSocketUrlDeclaration,
  // Poor man's minification
  bundle.replace(/\n +/g, "\n"),
].join("\n");

Deno.writeTextFileSync("dist/example/main.js", code);

console.log(`Wrote ${code.length} bytes`);

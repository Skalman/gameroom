#!/usr/bin/env sh

set -e

deno lint --unstable --ignore='src/client/example/vendor/,src/client/example/main.js,deno-cache/,dist/'

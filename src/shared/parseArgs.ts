export function parseArgs(args: string[]): Record<string, string | boolean> {
  const parsed: Record<string, string | boolean> = {};

  for (let i = 0; i < args.length; i++) {
    const [prefix, name, ...values] = args[i].split(/\-\-|=/);
    if (prefix !== "" || !name) continue;

    let value: string | true;
    if (values.length) {
      value = values.join("=");
    } else {
      const next = args[i + 1];
      if (next && !next.startsWith("--")) {
        value = next;
      } else {
        value = true;
      }
    }

    parsed[name] = value;
  }

  return parsed;
}

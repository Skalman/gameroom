export type MessageFromServer =
  | MessageFromServer.JoinInfo
  | MessageFromServer.ByeMessage
  | MessageFromServer.RoomInfo
  | MessageFromServer.Error;

// deno-lint-ignore no-namespace
export namespace MessageFromServer {
  export interface JoinInfo {
    type: "joinInfo";
    roomId: number;
    slug: string;
    playerId: number;
    playerName?: string;
    password: string;
    publicData?: string;
  }

  export interface ByeMessage {
    type: "bye";
  }

  export interface RoomInfo {
    type: "roomInfo";
    players?: Player[];
    publicData?: PublicData;
    publicLog?: PublicLog;
  }

  export interface Player {
    playerId: number;
    isActive: boolean;
    playerName?: string;
    publicData?: string;
  }

  export interface PublicData {
    editToken: string;
    payload?: string;
  }

  export interface PublicLog {
    editToken: string;
    items: PublicLogItem[];
  }

  export interface PublicLogItem {
    created: number;
    playerId: number;
    payload: string;
  }

  export interface Error {
    type: "error";
    reason:
      | "badMessageSerialization"
      | "internalServerError"
      | "hasNotJoinedRoom"
      | "badPassword"
      | "roomDoesntExist"
      | "kickedOut"
      | "playerDoesntExist"
      | "editConflict";
    message?: string;
  }
}

export type MessageToServer =
  | MessageToServer.JoinRoomMessage
  | MessageToServer.CreateRoomMessage
  | MessageToServer.LeaveRoomMessage
  | MessageToServer.SetMyInfo
  | MessageToServer.SetRoomInfo;

// deno-lint-ignore no-namespace
export namespace MessageToServer {
  export interface JoinRoomMessage {
    type: "join";
    idOrSlug: string | number;
    password?: string;
  }
  export interface CreateRoomMessage {
    type: "create";
  }
  export interface LeaveRoomMessage {
    type: "leave";
  }
  export interface SetMyInfo {
    type: "setMyInfo";
    name?: string;
    publicData?: string;
  }
  export interface SetRoomInfo {
    type: "setRoomInfo";
    publicData?: {
      editToken: string;
      payload?: string;
    };
    publicLog?: {
      /** If given, block adding to the log if there is an `editToken` mismatch. */
      editToken?: string;
      payload: string;
    };
  }
}

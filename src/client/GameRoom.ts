import { MessageFromServer, MessageToServer } from "../shared/messages.ts";
import { Listener, ListenerCollection } from "./ListenerCollection.ts";
import {
  PermanentWebSocket,
  WebSocketReconnectPolicy,
  WebSocketStatus,
} from "./PermanentWebSocket.ts";

export type MessageToMe<TData extends GameData> = MessageToMeRaw<
  TData["messagePayload"]
>;

interface MessageToMeRaw<T> {
  sender: number;
  payload: T;
}

type Factory<T> = () => T;

export interface RoomConnection {
  webSocket: string | Factory<WebSocket>;
  roomId?: number;
  slug?: string;
  password?: string;
  timeout?: number;
  reconnectPolicy?: Partial<WebSocketReconnectPolicy>;
}

export interface Player<TData extends GameData> {
  id: number;
  name?: string;
  isActive: boolean;
  publicData?: TData["playerPublicData"];
}

export interface GameRoomEventMap<TData extends GameData> {
  messageToMe: MessageToMe<TData>;
  player: readonly Player<TData>[];
  webSocketStatus: WebSocketStatus;
  error: GameRoomError;
  messageFromServer: MessageFromServer;
  stateChange: {
    webSocketStatus?: true;
    isInRoom?: true;
    connection?: true;
    myInfo?: true;
    players?: true;
    publicData?: true;
    publicLog?: true;
  };
}

export interface GameData {
  messagePayload: unknown;
  playerPublicData: unknown;
  roomPublicData: unknown;
  roomPublicLog: unknown;
}

export type GameRoomError =
  | MessageFromServer.Error
  | { type: "error"; reason: "webSocketError"; message: string; cause: Event };

export interface GameRoomPublicLogItem<T> {
  created: Date;
  playerId: number;
  payload: T;
}

export class GameRoom<TData extends GameData = GameData> {
  readonly webSocket: PermanentWebSocket;
  get webSocketStatus(): WebSocketStatus {
    return this.webSocket.status;
  }
  connection: RoomConnection;
  players: readonly Player<TData>[] = [];
  private readonly listeners = {
    messageFromServer: new ListenerCollection<MessageFromServer>(),
    messageToMe: new ListenerCollection<MessageToMe<TData>>(),
    player: new ListenerCollection<readonly Player<TData>[]>(),
    error: new ListenerCollection<GameRoomError>(),
    stateChange: new ListenerCollection<
      GameRoomEventMap<TData>["stateChange"]
    >(),
  } as const;
  isInRoom = false;
  publicData?: { editToken: string; payload: TData["roomPublicData"] };
  publicLog?: {
    editToken: string;
    items: GameRoomPublicLogItem<TData["roomPublicLog"]>[];
  };
  myInfo?: {
    playerId: number;
    playerName?: string;
    publicData?: TData["playerPublicData"];
  };

  constructor(connection: RoomConnection) {
    this.connection = { ...connection };

    this.webSocket = new PermanentWebSocket(
      connection.webSocket,
      connection.reconnectPolicy
    );
    this.listenToWebSocketMessages();
  }

  dispose(): void {
    this.webSocket.close();
  }

  private listenToWebSocketMessages() {
    this.webSocket
      .on("message", (data) => {
        if (typeof data !== "string") return;

        const splitIndex = data.indexOf(":");
        const sender = data.substr(0, splitIndex);
        const payload = data.substr(splitIndex + 1);

        if (sender === "server") {
          this.handleMessageFromServer(payload);
        } else {
          this.handleMessageToMe(+sender, payload);
        }
      })
      .on("status", (status) => {
        const { roomId, password } = this.connection;
        if (status === "open" && roomId && password) {
          this.join(roomId, password);
        }

        this.listeners.stateChange.next({ webSocketStatus: true });
      })
      .on("error", (error) => {
        this.listeners.error.next({
          type: "error",
          reason: "webSocketError",
          message: error.type,
          cause: error,
        });
      });
  }

  private handleMessageFromServer(payload: string) {
    const msg = jsonParseOr<MessageFromServer | void>(payload, (e) =>
      console.error(e, "Failed to parse server message")
    );
    if (!msg) return;

    switch (msg.type) {
      case "joinInfo":
        this.handleJoinInfoMessage(msg);
        break;

      case "bye":
        this.handleByeMessage();
        break;

      case "roomInfo":
        this.handleRoomInfoMessage(msg);
        break;

      case "error":
        this.listeners.error.next(msg);
        break;

      default: {
        const dummy: never = msg;
        return dummy;
      }
    }

    this.listeners.messageFromServer.next(msg);
  }

  private handleMessageToMe(sender: number, payload: string) {
    const parsed = jsonParseOr<TData["messagePayload"] | void>(payload, (e) =>
      console.error(e, `Failed to parse message to me from ${sender}`)
    );
    if (parsed === undefined) return;

    const message: MessageToMe<TData> = {
      payload: parsed,
      sender,
    };

    this.listeners.messageToMe.next(message);
  }

  private handleJoinInfoMessage(msg: MessageFromServer.JoinInfo) {
    this.isInRoom = true;
    this.connection.roomId = msg.roomId;
    this.connection.slug = msg.slug;
    this.connection.password = msg.password;
    this.myInfo = {
      playerId: msg.playerId,
      playerName: msg.playerName,
      publicData: jsonParseOr<TData["playerPublicData"] | undefined>(
        msg.publicData,
        (e) => console.error(e, "Unable to parse my public player data")
      ),
    };
    this.listeners.stateChange.next({
      isInRoom: true,
      connection: true,
      myInfo: true,
    });
  }

  private handleByeMessage() {
    this.isInRoom = false;
    this.listeners.stateChange.next({ isInRoom: true });
  }

  private handleRoomInfoMessage(msg: MessageFromServer.RoomInfo) {
    if (msg.players?.length) {
      const players = this.players.slice();
      for (const serverPlayer of msg.players) {
        const publicData = jsonParseOr<TData["playerPublicData"] | undefined>(
          serverPlayer.publicData,
          (e) =>
            console.error(
              e,
              "Unable to parse public player data for serverPlayer",
              serverPlayer.playerId
            )
        );

        const player: Player<TData> = {
          id: serverPlayer.playerId,
          name: serverPlayer.playerName,
          isActive: serverPlayer.isActive,
          publicData,
        };

        players[serverPlayer.playerId] = player;
      }

      this.players = players;
      this.listeners.player.next(players);
    }

    if (msg.publicData) {
      this.publicData = {
        editToken: msg.publicData.editToken,
        payload: jsonParseOr<TData["roomPublicData"] | undefined>(
          msg.publicData.payload,
          (e) => console.error(e, "Failed to parse public data")
        ),
      };
    }

    if (msg.publicLog) {
      let errorCount = 0;
      let items = msg.publicLog.items.map<
        GameRoomPublicLogItem<TData["roomPublicLog"]>
      >((x) => ({
        created: new Date(x.created),
        playerId: x.playerId,
        payload: jsonParseOr<TData["roomPublicLog"] | undefined>(
          x.payload,
          () => {
            errorCount++;
          }
        ),
      }));

      if (errorCount) {
        console.error(`Failed to parse ${errorCount} public log items`);
        items = items.filter((x) => x.payload !== undefined);
      }

      this.publicLog = {
        editToken: msg.publicLog.editToken,
        items,
      };
    }

    this.listeners.stateChange.next({
      players: msg.players && true,
      publicData: msg.publicData && true,
      publicLog: msg.publicLog && true,
    });
  }

  getNextJoinInfo(timeout?: number): Promise<MessageFromServer.JoinInfo> {
    return this.getNextMessageFromServer<MessageFromServer.JoinInfo>(
      (x) => x.type === "joinInfo",
      timeout
    );
  }

  getNextByeMessage(timeout?: number): Promise<MessageFromServer.ByeMessage> {
    return this.getNextMessageFromServer<MessageFromServer.ByeMessage>(
      (x) => x.type === "bye",
      timeout
    );
  }

  getNextErrorMessage(timeout?: number): Promise<MessageFromServer.Error> {
    return this.getNextMessageFromServer<MessageFromServer.Error>(
      (x) => x.type === "error",
      timeout
    );
  }

  getNextRoomInfo(timeout?: number): Promise<MessageFromServer.RoomInfo> {
    return this.getNextMessageFromServer<MessageFromServer.RoomInfo>(
      (x) => x.type === "roomInfo",
      timeout
    );
  }

  getNextMessageFromServer<T extends MessageFromServer>(
    validate?: (msg: T) => boolean,
    timeout = this.connection.timeout ?? 10_000
  ): Promise<T> {
    return this.listeners.messageFromServer.getNext(validate, timeout);
  }

  getNextMessage<T extends TData["messagePayload"]>(
    validate?: (msg: MessageToMeRaw<T>) => boolean,
    timeout = this.connection.timeout ?? 10_000
  ): Promise<MessageToMeRaw<T>> {
    return this.listeners.messageToMe.getNext(validate, timeout);
  }

  /**
   * Returns when the status next is the given status. If it already has the given status, or immediately if it
   * already has the given.
   */
  getNextStatus<T extends WebSocketStatus>(
    status: T,
    timeout = this.connection.timeout ?? 10_000
  ): Promise<T> {
    if (this.webSocketStatus === status) {
      return Promise.resolve(status);
    }

    return this.webSocket.getNextStatus(status, timeout);
  }

  getNextOpenStatus(
    timeout = this.connection.timeout ?? 10_000
  ): Promise<"open"> {
    return this.getNextStatus("open", timeout);
  }

  create(): this {
    this.players = [];
    this.listeners.player.next(this.players);
    this.webSocket.send('server:{"type":"create"}');
    return this;
  }

  /** Save join info when not in the room. Can be used for later rejoining. */
  saveIdOrSlug(idOrSlug: number | string, password?: string): this {
    if (this.isInRoom) {
      throw new Error("Can't set saved ID or slug when already in the room");
    }

    const [id, slug] =
      typeof idOrSlug === "number"
        ? [idOrSlug, undefined]
        : [undefined, idOrSlug];

    this.connection.roomId = id;
    this.connection.slug = slug;
    this.connection.password = password;
    this.listeners.stateChange.next({ connection: true });
    return this;
  }

  join(idOrSlug: number | string, password?: string): this {
    this.players = [];
    this.listeners.player.next(this.players);
    this.webSocket.send(
      `server:${JSON.stringify({ type: "join", idOrSlug, password })}`
    );
    this.listeners.stateChange.next({ players: true });
    return this;
  }

  rejoin(): this {
    if (!this.connection.roomId || !this.connection.password) {
      throw new Error("Can't rejoin, because no join info is available");
    }

    if (this.isInRoom) {
      this.leave();
    }

    return this.join(this.connection.roomId, this.connection.password);
  }

  leave(): this {
    this.webSocket.send('server:{"type":"leave"}');
    return this;
  }

  sendMessage(
    recipient: "all" | number,
    message: TData["messagePayload"]
  ): this {
    this.webSocket.send(`${recipient}:${JSON.stringify(message)}`);
    return this;
  }

  setMyInfo(name?: string, publicData?: TData["playerPublicData"]): this {
    const msg: MessageToServer = {
      type: "setMyInfo",
      name,
      publicData:
        publicData === undefined ? undefined : JSON.stringify(publicData),
    };
    this.webSocket.send(`server:${JSON.stringify(msg)}`);
    return this;
  }

  setRoomInfo({
    publicData,
    publicLog,
  }: {
    publicData?: TData["roomPublicData"];
    publicLog?: { payload: TData["roomPublicLog"]; withoutLock?: boolean };
  }) {
    if (!publicData && !publicLog) {
      throw new Error("Either publicData or publicLog must be given");
    }

    const msg: MessageToServer = {
      type: "setRoomInfo",
    };

    if (publicData) {
      if (!this.publicData) {
        throw new Error("Cannot set public data");
      }

      msg.publicData = {
        editToken: this.publicData.editToken,
        payload: JSON.stringify(publicData),
      };
    }

    if (publicLog) {
      if (publicLog.withoutLock) {
        msg.publicLog = {
          payload: JSON.stringify(publicLog.payload),
        };
      } else {
        if (!this.publicLog) {
          throw new Error("Cannot set public log");
        }
        msg.publicLog = {
          editToken: this.publicLog.editToken,
          payload: JSON.stringify(publicLog.payload),
        };
      }
    }

    this.webSocket.send(`server:${JSON.stringify(msg)}`);
    return this;
  }

  on<T extends keyof GameRoomEventMap<TData>>(
    event: T,
    listener: Listener<GameRoomEventMap<TData>[T]>
  ): this {
    switch (event) {
      case "webSocketStatus":
        this.webSocket.on("status", listener as Listener<unknown>);
        break;

      case "error":
      case "messageToMe":
      case "player":
      case "messageFromServer":
      case "stateChange":
        this.listeners[
          event as
            | "error"
            | "messageToMe"
            | "player"
            | "messageFromServer"
            | "stateChange"
        ].addListener(listener as Listener<unknown>);
        break;

      default:
        throw new Error(`Unknown event ${event}`);
    }

    return this;
  }

  off<T extends keyof GameRoomEventMap<TData>>(
    event: T,
    listener: Listener<GameRoomEventMap<TData>[T]>
  ): this {
    switch (event) {
      case "webSocketStatus":
        this.webSocket.off("status", listener as Listener<unknown>);
        break;

      case "error":
      case "messageToMe":
      case "player":
      case "messageFromServer":
      case "stateChange":
        this.listeners[
          event as
            | "error"
            | "messageToMe"
            | "player"
            | "messageFromServer"
            | "stateChange"
        ].removeListener(listener as Listener<unknown>);
        break;

      default:
        throw new Error(`Unknown event ${event}`);
    }

    return this;
  }
}

function jsonParseOr<T>(json: string | undefined, onError: (e: Error) => T): T {
  try {
    return json ? JSON.parse(json) : undefined;
  } catch (e) {
    return onError(e);
  }
}

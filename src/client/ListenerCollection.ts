export type Listener<T> = (event: T) => void;

export class ListenerCollection<TPayload> {
  private listeners = new Set<Listener<TPayload>>();

  addListener(listener: Listener<TPayload>) {
    this.listeners.add(listener);
  }

  removeListener(listener: Listener<TPayload>) {
    this.listeners.delete(listener);
  }

  next(payload: TPayload) {
    for (const listener of this.listeners) {
      listener(payload);
    }
  }

  getNext<TSpecific extends TPayload>(
    validate: ((value: TSpecific) => boolean) | undefined,
    timeout: number
  ): Promise<TSpecific> {
    return new Promise<TSpecific>((resolve, reject) => {
      const listener = ((value: TSpecific) => {
        if (validate === undefined || validate(value)) {
          this.listeners.delete(listener);
          clearTimeout(timeoutHandle);
          resolve(value);
        }
      }) as Listener<TPayload>;

      this.listeners.add(listener);

      const timeoutHandle = setTimeout(() => {
        this.listeners.delete(listener);
        reject(new Error("Timed out before message was received"));
      }, timeout);
    });
  }
}

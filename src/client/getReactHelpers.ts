import { GameRoomEventMap, GameData, GameRoom } from "./GameRoom.ts";
import { Listener } from "./ListenerCollection.ts";

interface ReactNamespace {
  useEffect(effect: () => () => void, deps?: unknown[]): void;
  useState<T>(initialState: () => T): [T, (newVal: (oldVal: T) => T) => void];
}

export function getReactHelpers({ useEffect, useState }: ReactNamespace) {
  return { useRoomEffect, useRoomState };

  function useRoomEffect<T extends keyof GameRoomEventMap<GameData>>(
    room: GameRoom,
    event: T,
    listener: Listener<GameRoomEventMap<GameData>[T]>
  ) {
    useEffect(() => {
      room.on(event, listener);

      return () => {
        room.off(event, listener);
      };
    }, [room]);
  }

  function useRoomState<T>(room: GameRoom, selector: (x: GameRoom) => T) {
    const [val, setVal] = useState(() => selector(room));

    useRoomEffect(room, "stateChange", () => {
      setVal((val) => {
        const selected = selector(room);
        return shallowEqual(val, selected) ? val : selected;
      });
    });

    return val;
  }
}

function shallowEqual<T>(a: T, b: T) {
  if (Object.is(a, b)) {
    return true;
  } else if (!a || !b || typeof a !== "object" || typeof b !== "object") {
    return false;
  } else if (Array.isArray(a) && Array.isArray(b)) {
    return a.length === b.length && a.every((x, i) => b[i] === x);
  } else {
    for (const key in a) {
      if (a[key] !== b[key]) {
        return false;
      }
    }
    return true;
  }
}

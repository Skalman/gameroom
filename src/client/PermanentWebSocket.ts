import { Listener, ListenerCollection } from "./ListenerCollection.ts";

export type WebSocketStatus =
  | "open"
  | "connecting"
  | "closing"
  | "closed"
  | "waitingForReconnect";

export interface WebSocketReconnectPolicy {
  enabled: boolean;
  initialBackoffInMilliseconds: number;
  backoffFactor: number;
}

interface PermanentWebSocketEventMap {
  message: string | Uint8Array;
  status: WebSocketStatus;
  error: Event;
}

export class PermanentWebSocket {
  status: WebSocketStatus = "closed";
  private readonly factory: () => WebSocket;
  private socket?: WebSocket;

  private readonly listeners = {
    message: new ListenerCollection<string | Uint8Array>(),
    status: new ListenerCollection<WebSocketStatus>(),
    error: new ListenerCollection<Event | boolean>(),
  } as const;

  private readonly reconnectPolicy: WebSocketReconnectPolicy;
  private currentBackoff?: number;
  private reconnectHandle?: number;

  constructor(
    urlOrFactory: string | (() => WebSocket),
    policy?: Partial<WebSocketReconnectPolicy>
  ) {
    this.factory =
      typeof urlOrFactory === "string"
        ? () => new WebSocket(urlOrFactory)
        : urlOrFactory;

    this.reconnectPolicy = {
      enabled: policy?.enabled ?? true,

      initialBackoffInMilliseconds:
        policy?.initialBackoffInMilliseconds ?? 1000,

      backoffFactor: policy?.backoffFactor ?? 2,
    };

    this.openInternal();
  }

  private setStatus(status: WebSocketStatus) {
    this.status = status;
    this.listeners.status.next(status);
  }

  open() {
    if (this.status !== "closed" && this.status !== "waitingForReconnect") {
      throw new Error(`Can't open socket from status ${this.status}`);
    }

    this.reconnectPolicy.enabled = true;
    clearTimeout(this.reconnectHandle);
    this.currentBackoff = undefined;
    this.openInternal();
  }

  private openInternal() {
    this.setStatus("connecting");
    this.socket = this.factory();
    this.addListeners(this.socket);

    if (this.socket.readyState === WebSocket.OPEN) {
      this.setStatus("open");
    }
  }

  close() {
    if (this.status === "closed" || !this.socket) return;

    this.reconnectPolicy.enabled = false;
    clearTimeout(this.reconnectHandle);
    this.socket.close();
    this.socket = undefined;
    this.setStatus("closing");
  }

  on<T extends keyof PermanentWebSocketEventMap>(
    event: T,
    listener: Listener<PermanentWebSocketEventMap[T]>
  ): this {
    this.listeners[event].addListener(listener as Listener<unknown>);
    return this;
  }

  off<T extends keyof PermanentWebSocketEventMap>(
    event: T,
    listener: Listener<PermanentWebSocketEventMap[T]>
  ): this {
    this.listeners[event].removeListener(listener as Listener<unknown>);
    return this;
  }

  getNextStatus<T extends WebSocketStatus>(
    status: T | undefined,
    timeout: number
  ): Promise<T> {
    return this.listeners.status.getNext(
      status ? (x) => x === status : undefined,
      timeout
    );
  }

  send(data: string | Uint8Array) {
    if (this.status !== "open") {
      throw new Error(
        `Cannot send data, because the current status is '${this.status}'`
      );
    }

    this.socket!.send(data);
  }

  private handleMessageEvent = ({ data }: MessageEvent) => {
    this.listeners.message.next(data);
  };

  private handleOpenEvent = () => {
    this.currentBackoff = undefined;
    this.setStatus("open");
  };

  private handleCloseEvent = () => {
    this.setStatus("closed");

    if (this.socket) {
      this.removeListeners(this.socket);
    }

    if (!this.reconnectPolicy.enabled) {
      this.setStatus("closed");
      return;
    }

    this.setStatus("waitingForReconnect");

    this.currentBackoff = this.currentBackoff
      ? this.currentBackoff * this.reconnectPolicy.backoffFactor
      : this.reconnectPolicy.initialBackoffInMilliseconds;

    this.reconnectHandle = setTimeout(() => {
      this.openInternal();
    }, this.currentBackoff);
  };

  private handleErrorEvent = (event: Event) => {
    this.listeners.error.next(event);
  };

  private addListeners(socket: WebSocket) {
    socket.addEventListener("message", this.handleMessageEvent);
    socket.addEventListener("open", this.handleOpenEvent);
    socket.addEventListener("close", this.handleCloseEvent);
    socket.addEventListener("error", this.handleErrorEvent);
  }

  private removeListeners(socket: WebSocket) {
    socket.removeEventListener("message", this.handleMessageEvent);
    socket.removeEventListener("open", this.handleOpenEvent);
    socket.removeEventListener("close", this.handleCloseEvent);
    socket.removeEventListener("error", this.handleErrorEvent);
  }
}

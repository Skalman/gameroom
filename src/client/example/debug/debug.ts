import { GameRoom } from "../../GameRoom.ts";

function set(id: string, value: unknown) {
  const elem = window.document.getElementById(id);
  elem!.textContent = JSON.stringify(value);
}

function $(tag: string, ...nodes: (string | Node)[]) {
  const elem = window.document.createElement(tag);
  elem.append(...nodes);
  return elem;
}

function log(prefix: string, value: unknown) {
  console.log(prefix, value);
  const elem = window.document.getElementById("log")!;
  elem.append(
    $(
      "div",
      $("small", new Date().toISOString().substr(11, 12)),
      " ",
      prefix,
      " ",
      JSON.stringify(value)
    )
  );
  elem.scrollTop += 1000;
}

export async function debug(url: string) {
  window.document.body.innerHTML = `
    <div class="status">Status: <b id="status"></b></div>
    <div id="log"></div>
  `;

  const room = new GameRoom({ webSocket: url });

  room
    .on("webSocketStatus", (status) => {
      log("status", status);
      set("status", status);
    })
    .on("error", (e) => log("error", e))
    .on("player", (player) => log("player", player))
    .on("messageToMe", (msg) => log("message", msg));

  await room.getNextOpenStatus();

  room.create();
  room.sendMessage(123, "hello!");
  room.leave();
}

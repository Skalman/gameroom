import { GameRoom, GameRoomError } from "../../GameRoom.ts";
import {
  React,
  ReactDOM,
  useState,
  useEffect,
  useRef,
} from "../vendor/react.ts";
import { getReactHelpers } from "../../getReactHelpers.ts";

const { useRoomEffect, useRoomState } = getReactHelpers(React);

export function chat(url: string) {
  ReactDOM.render(<Chat url={url} />, window.document.getElementById("root"));
}

function Chat({ url }: { url: string }) {
  const [room] = useState(() => new GameRoom({ webSocket: url }));
  const info = useRoomState(room, (x) => {
    return {
      status: x.webSocketStatus,
      slug: x.connection.slug,
      password: x.connection.password,
      isInRoom: x.isInRoom,
    };
  });

  useEffect(() => {
    if (room.connection.slug) return;

    const url = new URL(location.href);
    const urlRoom = url.searchParams.get("room");
    if (urlRoom) {
      room.saveIdOrSlug(urlRoom);
    }
  }, [room]);

  useEffect(() => {
    const url = new URL(location.href);
    if (info.slug) {
      url.searchParams.set("room", info.slug);
    } else {
      url.searchParams.delete("room");
    }

    window.history.replaceState(null, window.document.title, url.href);
  }, [info.slug]);

  useEffect(() => {
    if (info.status !== "open") return;

    const url = new URL(location.href);
    if (url.searchParams.get("autoconnect") === "1") {
      url.searchParams.delete("autoconnect");
      window.history.replaceState(null, window.document.title, url.href);

      if (info.slug && !info.isInRoom) {
        room.join(info.slug, info.password);
      }
    }
  }, [info.status]);

  return !info.isInRoom ? <JoinForm room={room} /> : <ChatRoom room={room} />;
}

function JoinForm({ room }: { room: GameRoom }) {
  const [slugOrId, setSlugOrId] = useState("");
  const [didInitSlug, setdidInitSlug] = useState(false);
  const [lastError, setLastError] = useState<GameRoomError>();

  const rejoinInfo = useRoomState(room, (x) => {
    const { slug, password } = x.connection;
    return slug ? { slug, password } : undefined;
  });

  useEffect(() => {
    if (!didInitSlug && rejoinInfo) {
      setdidInitSlug(true);
      setSlugOrId(rejoinInfo.slug);
    }
  }, [didInitSlug, rejoinInfo?.slug]);

  useRoomEffect(room, "error", (error) => setLastError(error));

  return (
    <form onSubmit={(e) => (e.preventDefault(), room.join(slugOrId!))}>
      <ul className="list-inline">
        {rejoinInfo?.password && (
          <li>
            <button
              type="button"
              onClick={() => room.join(rejoinInfo.slug, rejoinInfo.password!)}
            >
              Rejoin <b>{rejoinInfo.slug}</b>
            </button>
          </li>
        )}
        <li>
          <button type="button" onClick={() => room.create()}>
            Create
          </button>
        </li>
        <li>
          <input
            placeholder="Room ID"
            value={slugOrId}
            onChange={(x) => setSlugOrId(x.target.value)}
          />
          <button type="submit" disabled={!slugOrId}>
            Join
          </button>
        </li>
      </ul>
      {lastError && (
        <div>
          <strong>{lastError.reason}</strong> {lastError.message}
        </div>
      )}
    </form>
  );
}

function ChatRoom({ room }: { room: GameRoom }) {
  return (
    <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <ChatRoomTitle room={room} />
      <ChatRoomInvitation room={room} />
      <NameForm room={room} />
      <div
        style={{ flexGrow: 1, display: "flex", flexDirection: "row-reverse" }}
      >
        <ParticipantList room={room} />
        <div style={{ flexGrow: 1, display: "flex", flexDirection: "column" }}>
          <div style={{ flexGrow: 1, height: 0 }}>
            <MessageList room={room} />
          </div>
          <MessageForm room={room} />
        </div>
      </div>
    </div>
  );
}

function ChatRoomTitle({ room }: { room: GameRoom }) {
  const slug = useRoomState(room, (x) => x.connection.slug);
  return (
    <h1>
      {slug} <button onClick={() => room.leave()}>Leave</button>
    </h1>
  );
}

function ChatRoomInvitation({ room }: { room: GameRoom }) {
  const slug = useRoomState(room, (x) => x.connection.slug);
  if (!slug) {
    return null;
  }

  const joinUrl = new URL(location.href);
  joinUrl.searchParams.set("room", slug);
  joinUrl.searchParams.set("autoconnect", "1");
  return (
    <p>
      Others can join this chat room using the room ID <i>{slug}</i>.{" "}
      <a
        href={joinUrl.href}
        onClick={(e) => {
          if (
            e.button === 0 &&
            !e.altKey &&
            !e.ctrlKey &&
            !e.shiftKey &&
            !e.metaKey
          ) {
            e.preventDefault();
            window.navigator.clipboard.writeText(joinUrl.href);
          }
        }}
      >
        Copy link
      </a>
    </p>
  );
}

function NameForm({ room }: { room: GameRoom }) {
  const savedName = useRoomState(room, (x) => x.myInfo?.playerName);
  const [name, setMyName] = useState(savedName || "");
  const [state, setState] = useState<"show" | "edit" | "saving">(
    savedName ? "show" : "edit"
  );

  useRoomEffect(room, "messageFromServer", (msg) => {
    if (msg.type === "joinInfo") {
      setState((x) => (x === "saving" ? "show" : x));
    }
  });

  if (state === "show") {
    return (
      <a href="" onClick={(e) => (e.preventDefault(), setState("edit"))}>
        {savedName}
      </a>
    );
  } else {
    return (
      <form
        onSubmit={(e) => (
          e.preventDefault(), room.setMyInfo(name), setState("saving")
        )}
      >
        <input
          placeholder="Nickname"
          value={name}
          onChange={(e) => setMyName(e.target.value)}
        />
        <button type="submit" disabled={!name}>
          Save nickname
        </button>
      </form>
    );
  }
}

function ParticipantList({ room }: { room: GameRoom }) {
  const info = useRoomState(room, (x) => {
    return {
      playerId: x.myInfo?.playerId,
      players: room.players,
    };
  });

  return (
    <ul className="participant-list">
      {info.players.map((x) => (
        <li
          key={x.id}
          className={[
            x.isActive ? undefined : "text-muted",
            x.id === info.playerId ? "font-weight-bold" : undefined,
          ].join(" ")}
        >
          {x.name || <i>Anonymous</i>} <small>(#{x.id})</small>
        </li>
      ))}
    </ul>
  );
}

function MessageList({ room }: { room: GameRoom }) {
  type Message =
    | { type: "msg"; sender: number; text: string }
    | { type: "err"; text: string };

  const [messages, setMessages] = useState<Message[]>([]);
  const players = useRoomState(room, (x) => x.players);
  const scrollableElem = useRef<HTMLUListElement>(null);

  useRoomEffect(room, "messageToMe", (msg) => {
    setMessages((messages) => [
      ...messages,
      { type: "msg", sender: msg.sender, text: msg.payload + "" },
    ]);
  });

  useRoomEffect(room, "error", (error) => {
    setMessages((messages) => [
      ...messages,
      { type: "err", text: `${error.reason} ${error.message || ""}` },
    ]);
  });

  useEffect(() => {
    if (scrollableElem.current) {
      scrollableElem.current.scrollTop = 1e6;
    }
  }, [messages]);

  return (
    <ul className="message-list" ref={scrollableElem}>
      {messages.map((x, i) => (
        <li key={i}>
          {x.type === "msg" ? (
            <b>{players[x.sender]?.name ?? `#${x.sender}`}</b>
          ) : (
            <b style={{ color: "#c00" }}>ERROR</b>
          )}
          <div style={{ whiteSpace: "pre-wrap" }}>{x.text + ""}</div>
        </li>
      ))}
    </ul>
  );
}

function MessageForm({ room }: { room: GameRoom }) {
  const [message, setMessage] = useState("");

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        room.sendMessage("all", message);
        setMessage("");
      }}
      style={{ display: "flex" }}
    >
      <input
        value={message}
        style={{ padding: "1em", flexGrow: 1 }}
        placeholder="Type a message"
        onChange={(e) => setMessage(e.target.value)}
      />
      <button type="submit" disabled={!message}>
        Send
      </button>
    </form>
  );
}

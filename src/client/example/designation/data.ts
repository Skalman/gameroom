import { nouns } from "../../../assets/nouns.js";

export const wordLists = { en: nouns } as const;
export const otherTeam = { a: "b", b: "a" } as const;

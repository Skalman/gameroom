import { useState, useEffect } from "../vendor/react.ts";
import { DesignationGame } from "./DesignationGame.ts";
import { GameState } from "./models.ts";

export function useGameState<T>(
  game: DesignationGame,
  selector: (x: GameState) => T
): T;
export function useGameState(game: DesignationGame): GameState;
export function useGameState<T>(
  game: DesignationGame,
  selector: (x: GameState) => T = (x) => (x as unknown) as T
) {
  const [val, setVal] = useState(() => selector(game.state));

  useEffect(() => {
    game.addListener(listener);

    return () => {
      game.removeListener(listener);
    };
  }, [game]);

  function listener() {
    setVal((val) => {
      const selected = selector(game.state);
      return shallowEqual(val, selected) ? val : selected;
    });
  }

  return val;
}

function shallowEqual<T>(a: T, b: T) {
  if (Object.is(a, b)) {
    return true;
  } else if (!a || !b || typeof a !== "object" || typeof b !== "object") {
    return false;
  } else if (Array.isArray(a) && Array.isArray(b)) {
    return a.length === b.length && a.every((x, i) => b[i] === x);
  } else {
    for (const key in a) {
      if (a[key] !== b[key]) {
        return false;
      }
    }
    return true;
  }
}

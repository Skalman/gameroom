import { GameData, GameRoomPublicLogItem, Player } from "../../GameRoom.ts";
import { WebSocketStatus } from "../../PermanentWebSocket.ts";
import { FsmState } from "./fsm.ts";

export interface DesignationGameOptions {
  webSocketUrl: string;
  getSlug(): string | undefined;
  setSlug(slug: string | undefined): void;
  getState(): { wordLists?: WordList[]; wordListOwn?: string[] } | undefined;
  setState(state: { wordLists: string[]; wordListOwn: string[] }): void;
}

export type WordList = "en" | "own";

export interface GameState {
  slug?: string;
  me?: {
    playerId: number;
    name: string;
    team: "a" | "b";
    otherTeam: "a" | "b";
  };
  error?: string[];
  state: FsmState;
  wordList: {
    additionalWordsRequired: number;
    selected: WordList[];
    ownWords: string[];
  };
  roomPublicData?: RoomPublicData;
  roomPublicLog?: GameRoomPublicLogItem<RoomPublicLogItem>[];
  players?: readonly Player<DesignationGameData>[];
  playersByTeam?: {
    a: readonly Player<DesignationGameData>[];
    b: readonly Player<DesignationGameData>[];
  };
  calculated?: {
    currentTurn?: CurrentTurn;
    targetsLeftCount: number;
    turnsLeftCount: number;
    words: Word[];
    log: CalculatedLogItem[];
  };
  webSocketStatus: WebSocketStatus;
}

export interface CurrentTurn {
  index: number;
  hint: string;
  number: number;
  team: "a" | "b";
}

export interface Word {
  word: string;
  display: "target" | "neutral" | "bomb";
  isConsumed: boolean;
  isConsumedForTeams: ("a" | "b")[];
  guesses: WordGuess[];
  considering: Player<DesignationGameData>[];
}

export interface WordGuess {
  team: "we" | "they";
  result: "target" | "neutral" | "bomb";
}

export interface RoomPublicData {
  words: string[];
  teams: {
    a: { targets: number[]; bombs: number[] };
    b: { targets: number[]; bombs: number[] };
  };
  totalTurns: number;
}

export type RoomPublicLogItem = LogItem.RoomPublic;
export type CalculatedLogItem = LogItem.Calculated;
// deno-lint-ignore no-namespace
namespace LogItem {
  export type RoomPublic = Hint | Guess | EndGuessing;
  export type Calculated =
    | HintCalculated
    | GuessCalculated
    | EndGuessingCalculated;

  interface Hint {
    type: "hint";
    team: "a" | "b";
    hint: string;
    number: number;
  }

  interface Guess {
    type: "guess";
    team: "a" | "b";
    wordIndex: number;
  }

  interface EndGuessing {
    type: "endGuessing";
    team: "a" | "b";
  }

  interface HintCalculated extends Hint {
    playerId: number;
  }

  interface GuessCalculated extends Guess {
    playerId: number;
    result: "target" | "neutral" | "bomb";
  }

  interface EndGuessingCalculated extends EndGuessing {
    playerId: number;
  }
}

export interface DesignationGameData extends GameData {
  messagePayload: unknown;
  playerPublicData: PlayerPublicData;
  roomPublicData: RoomPublicData;
  roomPublicLog: RoomPublicLogItem;
}

interface PlayerPublicData {
  team: "a" | "b";
  considering?: {
    forTurn: number;
    words: number[];
  };
}

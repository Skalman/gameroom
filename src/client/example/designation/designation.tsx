import { React, ReactDOM } from "../vendor/react.ts";
import { Designation } from "./components/Designation.tsx";

export function designation(url: string) {
  ReactDOM.render(
    <Designation url={url} />,
    window.document.getElementById("root")
  );
}

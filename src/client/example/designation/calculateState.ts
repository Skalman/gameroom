import { otherTeam } from "./data.ts";
import { FsmEvent } from "./fsm.ts";
import { CalculatedLogItem, CurrentTurn, GameState, Word } from "./models.ts";

export function calculateState(
  state: GameState
):
  | { transition: FsmEvent; calculated: GameState["calculated"] }
  | { error: string } {
  const { me, roomPublicData, roomPublicLog, players } = state;
  if (!me || !roomPublicData || !roomPublicLog || !players) {
    const all = { me, roomPublicData, roomPublicLog, players };
    const missing = Object.keys(all)
      .filter((key) => !all[key as keyof typeof all])
      .join(", ");

    return {
      error: `Internal error: attempted to calculate state, but some data is missing: ${missing}`,
    };
  }

  const { team } = me;

  let currentTurnIndex = 0;
  let currentTurn: CurrentTurn | undefined;
  const targetsLeft = {
    a: new Set(roomPublicData.teams.a.targets),
    b: new Set(roomPublicData.teams.b.targets),
  };
  let foundBomb = false;

  const q = new Queue(
    roomPublicLog.map((x) => ({ ...x.payload, playerId: x.playerId }))
  );
  const words = roomPublicData.words.map<Word>((word, wordIndex) => ({
    word,
    display: roomPublicData.teams[team].bombs.includes(wordIndex)
      ? "bomb"
      : roomPublicData.teams[team].targets.includes(wordIndex)
      ? "target"
      : "neutral",
    isConsumed: false,
    isConsumedForTeams: [],
    guesses: [],
    considering: [],
  }));
  const log: CalculatedLogItem[] = [];

  // Each iteration consumes a whole turn.
  while (q.peek()) {
    const hint = q.pop();
    if (hint?.type !== "hint") {
      return {
        error: `Internal error: unexpected room log item type: ${hint?.type}`,
      };
    }

    log.push(hint);
    const relevantCards = roomPublicData.teams[hint.team];
    currentTurn = {
      index: currentTurnIndex,
      hint: hint.hint,
      number: hint.number,
      team: otherTeam[hint.team],
    };
    const teamRelative = currentTurn.team === team ? "we" : "they";
    let isTurnComplete = false;

    // Consume guesses.
    let lastGuessPlayerId: number | undefined;
    for (let guesses = 0; q.peek()?.type === "guess"; ) {
      const guess = q.pop();
      if (guess?.type !== "guess") throw new Error("Internal error");
      lastGuessPlayerId = guess.playerId;
      const wordIndex = guess.wordIndex;
      const word = words[wordIndex];

      foundBomb = relevantCards.bombs.includes(wordIndex);
      const foundTarget = relevantCards.targets.includes(wordIndex);
      const result = foundBomb ? "bomb" : foundTarget ? "target" : "neutral";
      word.guesses.push({ team: teamRelative, result });
      log.push({ ...guess, result });

      if (foundBomb) {
        isTurnComplete = true;
        word.isConsumed = true;
        word.isConsumedForTeams.push("a", "b");
        break;
      }

      if (!foundTarget) {
        isTurnComplete = true;

        if (teamRelative === "we") {
          word.isConsumed = true;
        }
        word.isConsumedForTeams.push(currentTurn.team);
        break;
      }

      targetsLeft.a.delete(wordIndex);
      targetsLeft.b.delete(wordIndex);
      word.isConsumed = true;
      word.isConsumedForTeams.push("a", "b");
      guesses++;

      if (guesses === hint.number + 1) {
        isTurnComplete = true;
        break;
      }
    }

    if (targetsLeft[currentTurn.team].size === 0) {
      isTurnComplete = true;
    }

    // Possibly consume endGuessing.
    if (q.peek()?.type === "endGuessing") {
      isTurnComplete = true;
      const endGuessing = q.pop();
      if (endGuessing?.type !== "endGuessing")
        throw new Error("Internal error");
      log.push(endGuessing);
    } else if (isTurnComplete && !foundBomb && lastGuessPlayerId) {
      log.push({
        type: "endGuessing",
        playerId: lastGuessPlayerId,
        team: currentTurn.team,
      });
    }

    if (isTurnComplete) {
      currentTurnIndex++;
      currentTurn = undefined;
    }

    if (foundBomb) {
      break;
    }
  }

  const targetsLeftCount = new Set([...targetsLeft.a, ...targetsLeft.b]).size;
  const turnsLeftCount = roomPublicData.totalTurns - currentTurnIndex;

  let transition: FsmEvent;
  if (foundBomb) {
    transition = "toLose";
  } else if (targetsLeftCount === 0) {
    transition = "toWin";
  } else if (roomPublicData.totalTurns <= currentTurnIndex) {
    transition = "toLose";
  } else if (currentTurn) {
    transition = currentTurn.team === team ? "toYouGuess" : "toTheyGuess";
  } else {
    if (targetsLeft.a.size > 0 && targetsLeft.b.size > 0) {
      transition = "toGiveHint";
    } else if (targetsLeft[team].size > 0) {
      transition = "toYouGiveHint";
    } else {
      transition = "toTheyGiveHint";
    }
  }

  for (const player of players) {
    if (!player.publicData) continue;
    const { team, considering } = player.publicData;
    if (team !== currentTurn?.team) continue;
    if (considering?.forTurn !== currentTurnIndex) continue;

    for (const wordIndex of considering.words) {
      if (!words[wordIndex].isConsumedForTeams.includes(currentTurn.team)) {
        words[wordIndex].considering.push(player);
      }
    }
  }

  return {
    calculated: {
      currentTurn,
      targetsLeftCount,
      turnsLeftCount,
      words,
      log,
    },
    transition,
  };
}

class Queue<T> {
  private pointer = 0;
  constructor(private readonly items: T[]) {}

  peek(): T | undefined {
    return this.items[this.pointer];
  }

  pop(): T | undefined {
    return this.items[this.pointer++];
  }
}

import "../test-infrastructure/mod.ts";
import { Draft } from "https://cdn.skypack.dev/immer@9.0.1?dts";
import { calculateState } from "./calculateState.ts";
import { GameState, RoomPublicLogItem } from "./models.ts";
import { assert } from "../test-infrastructure/mod.ts";

Deno.test("calculateState - other team's target", () => {
  const state = stateBuilder()
    .withHint({ playerId: 0, number: 1 })
    .withGuess({ playerId: 1, wordIndex: 5, type: "a=target, b=target" })
    .build();

  const result = calculateState(state);

  assert(!("error" in result), "Should not be error");
  assert(result.calculated?.words[5].isConsumed, "Should be consumed");
  assert(result.transition === "toTheyGuess", "Should transition toTheyGuess");
});

Deno.test("calculateState - they guess neutral", () => {
  const state = stateBuilder()
    .withHint({ playerId: 0, number: 1 })
    .withGuess({ playerId: 1, wordIndex: 20, type: "a=neutral, b=neutral" })
    .build();

  const result = calculateState(state);

  assert(!("error" in result), "Should not be error");
  assert(!result.calculated?.words[20].isConsumed, "Should not be consumed");
  assert(result.transition === "toGiveHint", "Should transition toGiveHint");
});

Deno.test("calculateState - we guess neutral", () => {
  const state = stateBuilder()
    .withHint({ playerId: 1, number: 1 })
    .withGuess({ playerId: 0, wordIndex: 20, type: "a=neutral, b=neutral" })
    .build();

  const result = calculateState(state);

  assert(!("error" in result), "Should not be error");
  assert(result.calculated?.words[20].isConsumed, "Should be consumed");
  assert(result.transition === "toGiveHint", "Should transition toGiveHint");
});

Deno.test("calculateState - same neutral guess", () => {
  const state = stateBuilder()
    .withHint({ playerId: 0, number: 1 })
    .withGuess({ playerId: 1, wordIndex: 20, type: "a=neutral, b=neutral" })
    .withEndGuessing(1)

    .withHint({ playerId: 1, number: 1 })
    .withGuess({ playerId: 0, wordIndex: 20, type: "a=neutral, b=neutral" })
    .withEndGuessing(0)

    .build();

  const result = calculateState(state);

  assert(!("error" in result), "Should not be error");
  assert(result.calculated?.words[20].isConsumed, "Should be consumed");
  assert(result.transition === "toGiveHint", "Should transition toGiveHint");
});

// prettier-ignore
const wordNumbers = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
  14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
] as const;

type WordNumbers = typeof wordNumbers[number];

type WordString = `W${WordNumbers}`;

function stateBuilder() {
  const players = [
    { id: 0, isActive: true, name: "player-0", publicData: { team: "a" } },
    { id: 1, isActive: true, name: "player-1", publicData: { team: "b" } },
  ] as const;

  const teamData = {
    a: {
      bombs: [0, 1, 2],
      targets: [3, 4, 5, 6, 7, 8, 9, 10, 11],
    },
    b: {
      bombs: [0, 3, 12],
      targets: [1, 4, 5, 6, 13, 14, 15, 16, 17],
    },
  } as const;

  type TeamData = typeof teamData;
  type WordTypeByNumberForTeamA = {
    [X in WordNumbers]: X extends TeamData["a"]["bombs"][number]
      ? "bomb"
      : X extends TeamData["a"]["targets"][number]
      ? "target"
      : "neutral";
  };
  type WordTypeByNumberForTeamB = {
    [X in WordNumbers]: X extends TeamData["b"]["bombs"][number]
      ? "bomb"
      : X extends TeamData["b"]["targets"][number]
      ? "target"
      : "neutral";
  };

  const teams = players.map((x) => x.publicData.team);
  type PlayerId = typeof players[number]["id"];

  const state: GameState = {
    webSocketStatus: "closed",
    state: "playing",
    me: { name: "player-0", playerId: 0, team: "a", otherTeam: "b" },
    players,
    wordList: { selected: [], ownWords: [], additionalWordsRequired: 0 },
    roomPublicData: {
      teams: teamData as Draft<typeof teamData>,
      totalTurns: 9,
      words: [...Array(25)].map<WordString>(
        (_, i) => `W${i as WordNumbers}` as const
      ),
    },
    roomPublicLog: [],
  };

  return {
    withLogItem(playerId: PlayerId, payload: RoomPublicLogItem) {
      state.roomPublicLog?.push({ created: new Date(), playerId, payload });
      return this;
    },

    withHint({
      playerId,
      number,
      hint,
    }: {
      playerId: PlayerId;
      number: number;
      hint?: string;
    }) {
      return this.withLogItem(playerId, {
        type: "hint",
        team: teams[playerId],
        hint: hint ?? Math.random().toString(),
        number,
      });
    },

    withGuess<T extends WordNumbers>({
      playerId,
      wordIndex,
    }: {
      playerId: PlayerId;
      wordIndex: T;
      type: `a=${WordTypeByNumberForTeamA[T]}, b=${WordTypeByNumberForTeamB[T]}`;
    }) {
      return this.withLogItem(playerId, {
        type: "guess",
        team: teams[playerId],
        wordIndex,
      });
    },

    withEndGuessing(playerId: PlayerId) {
      return this.withLogItem(playerId, {
        type: "endGuessing",
        team: teams[playerId],
      });
    },

    build() {
      return state;
    },
  };
}

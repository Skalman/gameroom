import fsm from "https://cdn.skypack.dev/micro-fsm@0.1.7?dts";

export type Fsm = ReturnType<typeof makeFsm>;
export type FsmState = Fsm["current"];
export type FsmEvent = Exclude<keyof Fsm, "current" | "prev">;

const playingStates = [
  "playing",
  "giveHint",
  "youGiveHint",
  "theyGiveHint",
  "youGuess",
  "theyGuess",
] as const;

export function isPlayingState(state: FsmState) {
  return playingStates.includes(
    state as typeof playingStates extends readonly (infer T)[] ? T : never
  );
}

export function makeFsm() {
  return fsm("init")
    .event("prepareRoom", { from: ["init"] })
    .event("createRoom", { from: ["init"], to: "connecting" })
    .event("joinRoom", { from: ["init"], to: "connecting" })
    .event("connectFailed", { from: ["connecting"], to: "init" })
    .event("connectSucceeded", { from: ["connecting"], to: "lobby" })
    .event("joinTeam", { from: ["lobby"], to: "joiningTeam" })
    .event("joinTeamFailed", { from: ["joiningTeam"], to: "lobby" })
    .event("joinTeamSucceeded", { from: ["joiningTeam"], to: "playing" })
    .event("giveHint", { from: ["giveHint", "youGiveHint"] })
    .event("toggleConsideration", { from: ["youGuess"] })
    .event("guess", { from: ["youGuess"] })
    .event("endGuessing", { from: ["youGuess"] })
    .event("toGiveHint", { from: playingStates, to: "giveHint" })
    .event("toYouGiveHint", { from: playingStates, to: "youGiveHint" })
    .event("toTheyGiveHint", { from: playingStates, to: "theyGiveHint" })
    .event("toYouGuess", { from: playingStates, to: "youGuess" })
    .event("toTheyGuess", { from: playingStates, to: "theyGuess" })
    .event("toWin", { from: playingStates, to: "win" })
    .event("toLose", { from: playingStates, to: "lose" })
    .event("runPlayingTransition", { from: playingStates })
    .build();
}

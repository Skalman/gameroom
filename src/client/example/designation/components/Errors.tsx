import { WebSocketStatus } from "../../../PermanentWebSocket.ts";
import { React } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { useGameState } from "../useGameState.ts";
import * as Ui from "./ui.tsx";

export function Errors({ game }: { game: DesignationGame }) {
  return (
    <>
      <WsError game={game} />
      <ErrorMessages game={game} />
    </>
  );
}

const wsStatusMessages: Record<WebSocketStatus, string | undefined> = {
  connecting: "Connecting...",
  open: undefined,
  waitingForReconnect: "Connection failed. Waiting to reconnect...",
  closing: "Closing connection",
  closed: "No connection",
};

function WsError({ game }: { game: DesignationGame }) {
  const status = useGameState(game, (x) => x.webSocketStatus);
  const message = wsStatusMessages[status];
  if (!message) return null;

  return (
    <Ui.ToastContainer position="top">
      <Ui.Toast header="Connection">{message}</Ui.Toast>
    </Ui.ToastContainer>
  );
}

function ErrorMessages({ game }: { game: DesignationGame }) {
  const errors = useGameState(game, (x) => x.error);
  if (!errors) return null;

  return (
    <Ui.ToastContainer position="topEnd">
      <Ui.Toast header="Error" onClose={() => game.dismissErrors()}>
        {errors.map((x, i) => (
          <p key={i}>{x}</p>
        ))}
      </Ui.Toast>
    </Ui.ToastContainer>
  );
}

import { React } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { useGameState } from "../useGameState.ts";
import { GameLog } from "./GameLog.tsx";
import { GameStateDisplay } from "./GameStateDisplay.tsx";
import { GameStats } from "./GameStats.tsx";
import { Hint } from "./Hint.tsx";
import { PlayerList } from "./PlayerList.tsx";
import { WordTable } from "./WordTable.tsx";
import * as Ui from "./ui.tsx";

export function Playing({ game }: { game: DesignationGame }) {
  const { calculated, state, players, playersByTeam, me } = useGameState(game);

  if (!calculated || !players || !playersByTeam || !me) {
    return null;
  }

  return (
    <div>
      <GameStateDisplay state={state} />
      <div className="d-flex">
        <div className="w-50 flex-grow-1">
          <WordTable game={game} words={calculated.words} />
          <Hint game={game} />
        </div>
        <div>
          <GameStats calculated={calculated} />
          <p>
            <Ui.ButtonOutlinePrimarySmall
              onClick={() => navigator.clipboard.writeText(location.href)}
            >
              Copy invitation link
            </Ui.ButtonOutlinePrimarySmall>
          </p>
          <h5>Your team</h5>
          <PlayerList
            players={playersByTeam[me.team]}
            highlightedPlayerId={me.playerId}
          />
          <h5>The other team</h5>
          <PlayerList players={playersByTeam[me.otherTeam]} />
          <GameLog game={game} />
        </div>
      </div>
    </div>
  );
}

import { React } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { useGameState } from "../useGameState.ts";
import { Connecting } from "./Connecting.tsx";
import { Lobby } from "./Lobby.tsx";
import { Playing } from "./Playing.tsx";
import { PrepareRoom } from "./PrepareRoom.tsx";

export function Main({ game }: { game: DesignationGame }) {
  const state = useGameState(game);

  switch (state.state) {
    case "init":
      return <PrepareRoom game={game} />;

    case "connecting":
      return <Connecting game={game} />;

    case "joiningTeam":
      return <div>Joining team... {JSON.stringify(state)}</div>;

    case "lobby":
      return <Lobby game={game} />;

    case "playing":
    case "giveHint":
    case "youGiveHint":
    case "theyGiveHint":
    case "youGuess":
    case "theyGuess":
    case "win":
    case "lose":
      return <Playing game={game} />;

    default: {
      const dummy: never = state.state;
      return <div>Internal error: state={dummy}</div>;
    }
  }
}

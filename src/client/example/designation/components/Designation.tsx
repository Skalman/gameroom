import { React } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { Errors } from "./Errors.tsx";
import { Header } from "./Header.tsx";
import { Main } from "./Main.tsx";
import * as Ui from "./ui.tsx";

export function Designation({ url }: { url: string }) {
  const game = new DesignationGame({
    webSocketUrl: url,
    getSlug: () => {
      const url = new URL(location.href);
      const urlRoom = url.searchParams.get("room");
      return urlRoom ?? undefined;
    },
    setSlug: (slug) => {
      const url = new URL(location.href);
      if (slug) {
        url.searchParams.set("room", slug);
      } else {
        url.searchParams.delete("room");
      }

      window.history.replaceState(null, window.document.title, url.href);
    },
    getState: () => {
      const json = window.localStorage.getItem("designation-words");
      if (json) {
        return JSON.parse(json);
      }
    },
    setState: (state) =>
      window.localStorage.setItem("designation-words", JSON.stringify(state)),
  });

  return (
    <>
      <Header game={game} />
      <Ui.Container>
        <Errors game={game} />
        <Main game={game} />
      </Ui.Container>
    </>
  );
}

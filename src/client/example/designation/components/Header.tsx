import { React } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { isPlayingState } from "../fsm.ts";
import { useGameState } from "../useGameState.ts";

export function Header({ game }: { game: DesignationGame }) {
  const state = useGameState(game, (x) => x.state);

  const { fontSize, margin } = isPlayingState(state)
    ? { fontSize: "fs-4", margin: "mb-2 mb-xl-3" }
    : { fontSize: "display-3", margin: "mb-3 mb-xl-5" };

  return (
    <header
      className={`header px-3 py-2 ${margin} bg-dark text-white text-center`}
    >
      <h1
        className={`d-inline-block transition ${fontSize}`}
        onClick={() => console.log("Game state", game.state)}
      >
        Designation
      </h1>
    </header>
  );
}

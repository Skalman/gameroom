import { React, useState } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { Word } from "../models.ts";
import { useGameState } from "../useGameState.ts";
import { PlayerList } from "./PlayerList.tsx";
import * as Ui from "./ui.tsx";
import { WordTable } from "./WordTable.tsx";

export function Lobby({ game }: { game: DesignationGame }) {
  const { roomPublicData, players } = useGameState(game);
  if (!roomPublicData || !players) return null;

  const words = React.useMemo(() => {
    return roomPublicData.words.map<Word>((word) => ({
      word,
      display: "neutral",
      guesses: [],
      isConsumed: false,
      isConsumedForTeams: [],
      considering: [],
    }));
  }, [roomPublicData.words]);

  const [name, setName] = useState("");

  return (
    <div>
      <Ui.Row className="justify-content-center">
        <Ui.Col size={4} className="text-center">
          <Ui.Input
            label="Nickname"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </Ui.Col>
      </Ui.Row>
      <Ui.Row className="justify-content-center text-center">
        <Ui.Col size={2}>
          <div className="mb-3">
            <Ui.ButtonPrimary
              type="button"
              disabled={!name}
              onClick={() => game.joinTeam(name, "a")}
            >
              Join team A
            </Ui.ButtonPrimary>
          </div>
          <PlayerList
            players={players.filter(
              (x) => x.isActive && x.publicData?.team === "a"
            )}
          />
        </Ui.Col>
        <Ui.Col size={2}>
          <div className="mb-3">
            <Ui.ButtonPrimary
              type="button"
              disabled={!name}
              onClick={() => game.joinTeam(name, "b")}
            >
              Join team B
            </Ui.ButtonPrimary>
          </div>
          <PlayerList
            players={players.filter(
              (x) => x.isActive && x.publicData?.team === "b"
            )}
          />
        </Ui.Col>
      </Ui.Row>
      <WordTable game={game} words={words} />
    </div>
  );
}

import { React, useEffect, useState } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";

export function GameStats({
  calculated,
}: {
  calculated: NonNullable<DesignationGame["state"]["calculated"]>;
}) {
  const { targetsLeftCount, turnsLeftCount } = calculated;
  const [initial] = useState(calculated);
  const [targetsHighlighted, setTargetsHighlighted] = useState("");
  const [turnsHighlighted, setTurnsHighlighted] = useState("");

  useEffect(() => {
    if (targetsLeftCount === initial.targetsLeftCount) return;
    setTargetsHighlighted("gamestats-highlight");
    const handle = setTimeout(() => setTargetsHighlighted(""), 1000);
    return () => clearTimeout(handle);
  }, [targetsLeftCount]);

  useEffect(() => {
    if (turnsLeftCount === initial.turnsLeftCount) return;
    setTurnsHighlighted("gamestats-highlight");
    const handle = setTimeout(() => setTurnsHighlighted(""), 1000);
    return () => clearTimeout(handle);
  }, [turnsLeftCount]);

  return (
    <>
      <div className="mb-3" title={`Turns left: ${turnsLeftCount}`}>
        <span className={`gamestats ${turnsHighlighted} rounded`}>
          {[...Array(turnsLeftCount)].map((_, i) => (
            <span key={i} className="bg-light border border-dark px-1 rounded">
              ❤️
            </span>
          ))}
        </span>
      </div>
      <div className="mb-3" title={`Words left to find: ${targetsLeftCount}`}>
        <span className={`gamestats ${targetsHighlighted} rounded`}>
          {[...Array(targetsLeftCount)].map((_, i) => (
            <span
              key={i}
              className="bg-success border border-dark px-1 rounded"
            >
              ⭐
            </span>
          ))}
        </span>
      </div>
    </>
  );
}

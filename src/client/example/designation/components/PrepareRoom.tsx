import { React, useState } from "../../vendor/react.ts";
import { arrayToggle } from "../arrayToggle.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { WordList } from "../models.ts";
import { useGameState } from "../useGameState.ts";
import * as Ui from "./ui.tsx";

const wordLists: { code: WordList; name: string }[] = [
  { code: "en", name: "English" },
  { code: "own", name: "Custom words" },
];

export function PrepareRoom({ game }: { game: DesignationGame }) {
  const selection = useGameState(game, (x) => x.wordList);
  const [ownWords, setOwnWords] = useState(() => selection.ownWords.join("\n"));

  return (
    <div>
      <form onSubmit={(e) => e.preventDefault()}>
        <Ui.Row className="justify-content-center">
          <Ui.Col size={4}>
            {selection.additionalWordsRequired ? (
              <Ui.AlertWarning>
                Need {selection.additionalWordsRequired} more words
              </Ui.AlertWarning>
            ) : null}
            <Ui.Label>Select dictionary</Ui.Label>
            {wordLists.map((x) => (
              <Ui.Checkbox
                key={x.code}
                label={x.name}
                checked={selection.selected.includes(x.code)}
                onChange={() =>
                  game.prepareRoom({
                    selectedWordLists: arrayToggle(selection.selected, x.code),
                  })
                }
              />
            ))}
            {selection.selected.includes("own") && (
              <>
                <label>Own words</label>{" "}
                <span className="form-text ms-1">(one word per line)</span>
                <Ui.Textarea
                  rows={12}
                  value={ownWords}
                  className="text-uppercase"
                  onChange={(e) => {
                    setOwnWords(e.target.value);
                    game.prepareRoom({
                      ownWords: [
                        ...new Set(
                          e.target.value
                            .toLocaleUpperCase()
                            .split("\n")
                            .filter((x) => x)
                            .sort()
                        ),
                      ],
                    });
                  }}
                />
              </>
            )}
          </Ui.Col>
          <Ui.Col size={4}>
            <Ui.ButtonPrimaryLarge
              disabled={selection.additionalWordsRequired > 0}
              onClick={() => game.createRoom()}
            >
              Create room
            </Ui.ButtonPrimaryLarge>
          </Ui.Col>
        </Ui.Row>
      </form>
    </div>
  );
}

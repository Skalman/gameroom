import { Player } from "../../../GameRoom.ts";
import { React, useEffect, useState } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import {
  DesignationGameData,
  Word as WordModel,
  WordGuess,
} from "../models.ts";

const colors = {
  target: "bg-success",
  bomb: "bg-dark",
  neutral: "word-bg-neutral",
};

export function Word({
  game,
  word,
  state,
}: {
  game: DesignationGame;
  word: WordModel;
  state: DesignationGame["state"]["state"];
}) {
  const color = colors[word.display];
  const canGuess = state === "youGuess" && !word.isConsumed;
  const neutralGuesses = word.guesses.filter((x) => x.result === "neutral");
  const finalGuess = word.guesses.find((x) => x.result !== "neutral");

  const [highlightClass, setHighlightClass] = useState("");

  useEffect(() => {
    if (!word.guesses.length) return;

    setHighlightClass("word-highlight");
    const handle = setTimeout(() => setHighlightClass(""), 1000);
    return () => clearTimeout(handle);
  }, [word.guesses.length]);

  return (
    <div
      className={`word card ${highlightClass} ${color} bg-gradient position-relative`}
    >
      <div className="word-word p-1 bg-light text-center text-uppercase fw-bold">
        {word.word}
      </div>
      <div className="position-absolute top-0 start-0 w-75 m-2">
        {neutralGuesses.map((guess, index) => (
          <NeutralGuessMarker key={index} {...guess} />
        ))}
        {word.considering.map((player) => (
          <ConsideringMarker key={player.id} player={player} />
        ))}
      </div>
      {canGuess && <GuessButtons game={game} word={word} />}
      {finalGuess && <FinalGuessCover {...finalGuess} />}
    </div>
  );
}

function GuessButtons({
  game,
  word,
}: {
  game: DesignationGame;
  word: WordModel;
}) {
  return (
    <>
      <button
        title={`Mark word ${word.word.toLocaleUpperCase()} for consideration`}
        className="btn position-absolute top-0 start-0 w-100 h-100"
        onClick={() => game.toggleConsideration(word.word)}
      ></button>
      <button
        title={`Guess ${word.word.toLocaleUpperCase()}`}
        className="btn btn-sm btn-warning position-absolute top-0 end-0 m-2 bg-gradient rounded-pill"
        onClick={() => game.guess(word.word)}
      >
        ✔️
      </button>
    </>
  );
}

function NeutralGuessMarker({ team }: WordGuess) {
  const rotate = rotateIfThey(team);

  return (
    <span
      className={`word-marker p-1 word-bg-neutral rounded-1 border border-secondary ${rotate}`}
    >
      😕
    </span>
  );
}

function ConsideringMarker({
  player,
}: {
  player: Player<DesignationGameData>;
}) {
  return (
    <span className="word-marker badge bg-light text-dark border">
      {player.name}
    </span>
  );
}

function FinalGuessCover({ result, team }: WordGuess) {
  const [isOpen, setIsOpen] = useState(false);
  const wordOpenClass = isOpen ? "word-open" : "";

  const btnBackground = result === "target" ? "btn-success" : "btn-dark";
  const rotate = rotateIfThey(team);

  return (
    <button
      className={`word-cover position-absolute top-0 start-0 h-100 w-100 bg-gradient rounded btn ${btnBackground} ${wordOpenClass}`}
      onClick={() => setIsOpen(!isOpen)}
      title="Show word"
    >
      {result === "target" ? (
        <span className={`d-inline-block fs-4 ${rotate}`}>⭐</span>
      ) : (
        <span className={`d-inline-block fs-2 ${rotate}`}>💣</span>
      )}
    </button>
  );
}

function rotateIfThey(team: "we" | "they") {
  return team === "we" ? "" : "rotate-180";
}

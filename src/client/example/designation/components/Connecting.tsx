import { React } from "../../vendor/react.ts";
import { DesignationGame } from "../DesignationGame.ts";
import { useGameState } from "../useGameState.ts";

export function Connecting({ game }: { game: DesignationGame }) {
  const state = useGameState(game);
  return <div>Connecting {JSON.stringify(state)}</div>;
}

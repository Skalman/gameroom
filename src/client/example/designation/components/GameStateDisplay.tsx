import { React } from "../../vendor/react.ts";
import { FsmState } from "../fsm.ts";

const messages: Record<FsmState, string | undefined> = {
  init: undefined,
  connecting: "Connecting to game...",
  lobby: undefined,
  joiningTeam: "Joining team...",
  playing: "Preparing game...",
  giveHint: "Somebody should give a hint",
  youGiveHint: "Your team should give a hint",
  youGuess: "Your team should guess",
  theyGiveHint: "The other team should give a hint",
  theyGuess: "The other team is guessing",
  win: "You win! 🥳",
  lose: "You lose. Better luck next time!",
};

export function GameStateDisplay({ state }: { state: FsmState }) {
  return <p className="fs-3 text-center">{messages[state]}</p>;
}

import { React, useState } from "../../vendor/react.ts";

export const AlertInfo = factory("div", "alert alert-info");
export const AlertWarning = factory("div", "alert alert-warning");
export const ButtonPrimary = factory("button", "btn btn-primary");
export const ButtonPrimaryLarge = factory("button", "btn btn-primary btn-lg");
export const ButtonOutlinePrimarySmall = factory(
  "button",
  "btn btn-sm btn-outline-primary"
);
export const Container = factory("div", "game-container");
export const Hero = factory("div", "p-4 my-5 col-lg-6 mx-auto");
export const InputGroup = factory("div", "input-group");
export const InputGroupText = factory("span", "input-group-text");
export const Label = factory("label", "form-label");
export const ListInline = factory("ul", "list-inline");
export const ListInlineItem = factory("li", "list-inline-item");
export const ListUnstyled = factory("ul", "list-unstyled");
export const Row = factory("div", "row");
export const Textarea = factory("textarea", "form-control");

let idCounter = 0;
function useId() {
  return useState(() => `c${idCounter++}`)[0];
}

export function Checkbox({
  label,
  ...props
}: JSX.IntrinsicElements["input"] & { label: string }) {
  const id = useId();
  return (
    <div className="form-check">
      <input type="checkbox" {...props} className="form-check-input" id={id} />
      <label className="form-check-label" htmlFor={id}>
        {label}
      </label>
    </div>
  );
}

export function Col({
  size,
  ...props
}: JSX.IntrinsicElements["input"] & { size?: number }) {
  return (
    <div
      {...props}
      className={combine(size ? `col-${size}` : "col", props.className)}
    />
  );
}

export function Input({
  label,
  ...props
}: JSX.IntrinsicElements["input"] & { label?: string }) {
  const cl = combine("form-control", props.className);
  if (!label) {
    return <input {...props} className={cl} />;
  }

  const id = useId();
  return (
    <div className="mb-3">
      <label className="form-label" htmlFor={id}>
        {label}
      </label>
      <input {...props} className={cl} id={id} />
    </div>
  );
}

const toastContainerPositions = {
  top: "top-0 start-50 translate-middle-x",
  topEnd: "top-0 end-0",
};

export function ToastContainer({
  position,
  children,
}: {
  position: keyof typeof toastContainerPositions;
  children?: React.ReactNode;
}) {
  const pos = toastContainerPositions[position];
  return (
    <div className={`position-fixed ${pos} p-3`} style={{ zIndex: 5 }}>
      {children}
    </div>
  );
}

export function Toast({
  header,
  small,
  onClose,
  children,
}: {
  header: string;
  small?: string;
  onClose?: () => void;
  children?: React.ReactNode;
}) {
  return (
    <div className="toast fade show bg-white text-dark">
      <div className="toast-header">
        <span
          className="d-inline-block bg-danger rounded me-2"
          style={{ width: "1.2em", height: "1.2em" }}
        ></span>
        <strong className="me-auto">{header}</strong>
        {small && <small>{small}</small>}
        {onClose && (
          <button
            type="button"
            className="btn-close"
            onClick={onClose}
          ></button>
        )}
      </div>
      <div className="toast-body">{children}</div>
    </div>
  );
}

function combine(a: string, b?: string) {
  return b ? `${a} ${b}` : a;
}

function factory<T extends keyof JSX.IntrinsicElements>(elem: T, cl: string) {
  // deno-lint-ignore no-explicit-any
  const Elem = elem as any;
  return function (props: JSX.IntrinsicElements[T]) {
    return <Elem {...props} className={combine(cl, props.className)} />;
  };
}

import "./test-infrastructure/mod.ts";
import { start } from "./main.ts";

Deno.test("build example", () => {
  // Just the fact that we're here, means that the example built correctly.
  if (typeof start !== "function") {
    throw new Error("start should be a function");
  }
});

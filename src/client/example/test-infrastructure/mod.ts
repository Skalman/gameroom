// Make this work both with a tsconfig.json with Deno and without.
declare global {
  // deno-lint-ignore no-namespace
  namespace Deno {
    function test(name: string, fn: () => void | Promise<void>): void;
  }
}

export function assert(x: unknown, message: string): asserts x {
  if (!x) {
    throw new Error(`Assertion failure: ${message}`);
  }
}

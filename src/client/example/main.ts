import { chat } from "./chat/chat.tsx";
import { debug } from "./debug/debug.ts";
import { designation } from "./designation/designation.tsx";

declare global {
  const gameRoomWebSocketUrl: string | undefined;
}

export function start(app: "chat" | "designation" | "debug") {
  const url =
    typeof gameRoomWebSocketUrl === "string"
      ? // deno-lint-ignore no-undef
        gameRoomWebSocketUrl
      : getDefaultWebSocketUrl();

  switch (app) {
    case "chat":
      chat(url);
      break;

    case "designation":
      designation(url);
      break;

    case "debug":
      debug(url);
      break;
  }
}

function getDefaultWebSocketUrl() {
  const protocol = location.protocol === "https:" ? "wss" : "ws";
  return `${protocol}://${location.host}/ws`;
}

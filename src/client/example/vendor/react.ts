// @deno-types="https://cdn.skypack.dev/pin/react@v17.0.1-yH0aYV1FOvoIPeKBbHxg/mode=types/index.d.ts"
import "./react.development.js";
// @deno-types="https://cdn.skypack.dev/pin/react-dom@v17.0.1-oZ1BXZ5opQ1DbTh7nu9r/mode=types/index.d.ts"
import "./react-dom.development.js";

export const React = window.React;
export const ReactDOM = window.ReactDOM;
export const { useState, useEffect, useRef } = React;

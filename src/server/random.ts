export function random(max: number) {
  return Math.floor(Math.random() * max);
}

export function randomString() {
  return Math.random().toString(36).substr(2);
}

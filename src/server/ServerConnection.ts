import {
  isWebSocketCloseEvent,
  isWebSocketPongEvent,
  WebSocket,
} from "https://deno.land/std@0.90.0/ws/mod.ts";
import {
  EnvelopeFromServer,
  EnvelopeToPlayer,
  EnvelopeToServer,
} from "./Envelope.ts";
import { RoomConnection, RoomConnectionCallbacks } from "./Room.ts";
import { RoomDatabase } from "./RoomDatabase.ts";

let id = 0;

export interface ConnectionOptions {
  webSocket: WebSocket;
  database: RoomDatabase;
  console?: Console;
}

export class ServerConnection {
  static connect(opts: ConnectionOptions): Promise<void> {
    return new ServerConnection(opts).connect();
  }

  private readonly ws: WebSocket;
  private readonly db: RoomDatabase;
  private readonly console: Console;

  private constructor(opts: ConnectionOptions) {
    this.ws = opts.webSocket;
    this.db = opts.database;
    this.console = opts.console ?? console;
  }

  private lastAlive = Date.now();
  private id = id++;
  private roomConnection?: RoomConnection;

  private async connect() {
    this.log("\\ connect");
    const timeoutHandle = this.sendPings();
    await this.listenToWebSocket();
    clearInterval(timeoutHandle);
    this.log("/ disconnect");
  }

  private log(...args: unknown[]) {
    this.console.log(this.id, ...args);
  }

  private async listenToWebSocket() {
    try {
      for await (const ev of this.ws) {
        this.lastAlive = Date.now();
        if (typeof ev === "string") {
          this.onText(ev);
        } else if (isWebSocketCloseEvent(ev)) {
          this.onClose();
        } else if (isWebSocketPongEvent(ev)) {
          this.log("| alive");
        }
      }
    } catch (err) {
      console.error("failed to receive frame", err);

      if (!this.ws.isClosed) {
        this.onClose();
        await this.ws.close(1000).catch(console.error);
      }
    }
  }

  private sendPings() {
    const pingInterval = 20_000;
    const pingIfLastActiveOlderThan = 10_000;

    const handle = setInterval(() => {
      if (this.ws.isClosed) {
        clearInterval(handle);
      } else if (this.lastAlive + pingIfLastActiveOlderThan / 2 < Date.now()) {
        this.ws.ping();
      }
    }, pingInterval);
    return handle;
  }

  private onText(ev: string) {
    const envelope =
      EnvelopeToServer.deserialize(ev) ?? EnvelopeToPlayer.deserialize(ev);

    if (!envelope) {
      this.ws.send(
        new EnvelopeFromServer({
          type: "error",
          reason: "badMessageSerialization",
        }).serialize()
      );
    } else if (envelope.prefix === "server") {
      this.onMessageToServer(envelope);
    } else {
      this.onMessageToPlayer(envelope);
    }
  }

  private getRoomConnectionCallbacks(): RoomConnectionCallbacks {
    return {
      receiveMessage: (envelope) => {
        if (this.ws.isClosed) return;

        this.ws.send(envelope.serialize());
      },
      kickOut: () => {
        this.roomConnection = undefined;
      },
    };
  }

  private onMessageToServer({ message }: EnvelopeToServer) {
    switch (message.type) {
      case "create": {
        this.roomConnection?.leave();
        const room = this.db.createRoom();
        this.roomConnection = room.join({
          callbacks: this.getRoomConnectionCallbacks(),
        });
        break;
      }

      case "join":
        try {
          this.roomConnection?.leave();
          const room = this.db.getRoom(message.idOrSlug);
          if (room) {
            if (message.password) {
              this.roomConnection = room.rejoin({
                password: message.password,
                callbacks: this.getRoomConnectionCallbacks(),
              });
            } else {
              this.roomConnection = room.join({
                callbacks: this.getRoomConnectionCallbacks(),
              });
            }
          } else {
            this.ws.send(
              new EnvelopeFromServer({
                type: "error",
                reason: "roomDoesntExist",
              }).serialize()
            );
          }
        } catch (e) {
          this.log("Failed to join room", e);
          this.ws.send(
            new EnvelopeFromServer({
              type: "error",
              reason: "internalServerError",
            }).serialize()
          );
          return;
        }
        break;

      case "leave":
        this.roomConnection?.leave();
        this.roomConnection = undefined;
        break;

      case "setMyInfo":
        this.roomConnection?.setMyInfo(message);
        break;

      case "setRoomInfo":
        this.roomConnection?.setRoomInfo(message);
        break;

      default: {
        const _dummy: never = message;
      }
    }
  }

  private onMessageToPlayer(envelope: EnvelopeToPlayer) {
    if (this.roomConnection) {
      this.roomConnection.sendMessage(envelope);
    } else {
      this.ws.send(
        new EnvelopeFromServer({
          type: "error",
          reason: "hasNotJoinedRoom",
        }).serialize()
      );
    }
  }

  private onClose() {
    this.roomConnection?.leave();
    this.roomConnection = undefined;
  }
}

import {
  serve,
  Server,
  ServerRequest,
} from "https://deno.land/std@0.90.0/http/server.ts";
import { acceptWebSocket } from "https://deno.land/std@0.90.0/ws/mod.ts";
import { join } from "https://deno.land/std@0.90.0/path/mod.ts";
import { parseArgs } from "../shared/parseArgs.ts";
import { RoomDatabase } from "./RoomDatabase.ts";
import { ServerConnection } from "./ServerConnection.ts";

const supportedExtensions = new Map([
  ["css", "text/css"],
  ["html", "text/html"],
  ["js", "text/javascript"],
  ["png", "image/png"],
]);

async function handlePublicFiles(publicPath: string, req: ServerRequest) {
  const reqUrl = req.url.replace(/\?.+/, "");

  const filePath = join(
    publicPath,
    "." + decodeURI(reqUrl) + (reqUrl.endsWith("/") ? "index.html" : "")
  );

  try {
    const extension = /\.([a-z]+)$/.exec(filePath)?.[1];
    const mimeType = extension && supportedExtensions.get(extension);
    if (!mimeType) {
      throw new Deno.errors.NotFound();
    }

    const [file, fileInfo] = await Promise.all([
      Deno.open(filePath),
      Deno.stat(filePath),
    ]);
    if (!fileInfo.isFile) {
      file.close();
      throw new Deno.errors.NotFound();
    }
    req.done.then(() => file.close());

    await req.respond({
      status: 200,
      body: file,
      headers: new Headers({
        "content-length": fileInfo.size.toString(),
        "content-type": mimeType,
      }),
    });
  } catch (e) {
    try {
      const headers = new Headers({ "content-type": "text/plain" });
      if (e instanceof Deno.errors.NotFound) {
        await req.respond({
          status: 404,
          body: "Not Found",
          headers,
        });
      } else {
        await req.respond({
          status: 500,
          body: "Internal server error",
          headers,
        });
      }
    } catch {
      // Do nothing
    }
  }
}

export interface Options {
  host?: string;
  publicPath?: string;
  webSocketPath?: string;
  databasePath?: string;
}

export function parseOptions(args: string[]): Options {
  const parsed = parseArgs(args);

  return {
    host: typeof parsed.host === "string" ? parsed.host : undefined,

    publicPath:
      typeof parsed["public-path"] === "string"
        ? parsed["public-path"]
        : undefined,

    webSocketPath:
      typeof parsed["websocket-path"] === "string"
        ? parsed["websocket-path"]
        : undefined,

    databasePath:
      typeof parsed.database === "string" ? parsed.database : undefined,
  };
}

async function arePermissionsGranted(
  ...descriptors: Deno.PermissionDescriptor[]
) {
  const permissions = await Promise.all(
    descriptors.map((x) => Deno.permissions.query(x))
  );

  return permissions.every((x) => x.state === "granted");
}

export async function start(
  {
    host = "localhost:80",
    publicPath = "public",
    webSocketPath = "/websocket",
    databasePath = "room.sqlite",
  } = parseOptions(Deno.args)
): Promise<Server> {
  const permissions = {
    net: await arePermissionsGranted({ name: "net", host }),
    readFiles: await arePermissionsGranted({ name: "read", path: publicPath }),
    db: await arePermissionsGranted(
      { name: "read", path: databasePath },
      { name: "write", path: databasePath },
      { name: "read", path: `${databasePath}-journal` },
      { name: "write", path: `${databasePath}-journal` }
    ),
  };

  if (!permissions.net) {
    throw new Error(`Permission denied. Run again with --allow-net=${host}`);
  }

  console.log(`Websocket server is running on http://${host}`);
  console.log(`  http://${host}${webSocketPath}`);
  console.log(`  http://${host}/debug`);

  if (permissions.readFiles) {
    console.log(`  Files are served from ${publicPath}`);
  } else {
    console.log(
      `  File server is disabled. Enable by running again with --allow-read=${publicPath}`
    );
  }

  let database: RoomDatabase;
  if (permissions.db) {
    database = new RoomDatabase(databasePath);
    console.log(`  Using database ${databasePath}`);
  } else {
    database = new RoomDatabase();
    console.log(
      `  Using in-memory database. Save to file by running again with --allow-read=${databasePath},${databasePath}-journal --allow-write=${databasePath},${databasePath}-journal`
    );
  }

  const server = serve(host);
  requestLoop();
  return server;

  async function requestLoop() {
    for await (const req of server) {
      const { conn, r: bufReader, w: bufWriter, headers, url } = req;
      if (url === webSocketPath) {
        acceptWebSocket({
          conn,
          bufReader,
          bufWriter,
          headers,
        })
          .then((ws) => ServerConnection.connect({ webSocket: ws, database }))
          .catch(async (err) => {
            console.error(`failed to accept websocket: ${err}`);
            await req.respond({ status: 400 });
          });
      } else if (/^\/debug(\/[0-9a-z-]+)?$/.test(url)) {
        const idOrSlug = /^\/debug\/(.+)$/.exec(url);
        req.respond({
          status: 200,
          body: JSON.stringify(database.getDebug(idOrSlug?.[1]) || null),
          headers: new Headers({
            "content-type": "application/json",
          }),
        });
      } else {
        if (permissions.readFiles) {
          handlePublicFiles(publicPath, req);
        } else {
          req.respond({
            status: 404,
            body: "Not found",
            headers: new Headers({ "content-type": "text/plain" }),
          });
        }
      }
    }

    database.dispose();
  }
}

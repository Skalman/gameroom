import { MessageFromServer, MessageToServer } from "../shared/messages.ts";

export abstract class Envelope<TPrefix extends string | number> {
  protected constructor(readonly prefix: TPrefix, readonly payload: string) {}

  serialize() {
    return `${this.prefix}:${this.payload}`;
  }
}

function deserializeEnvelope(envelope: string) {
  const splitIndex = envelope.indexOf(":");
  const prefix = envelope.substr(0, splitIndex);
  const payload = envelope.substr(splitIndex + 1);

  if (prefix && payload) {
    return { prefix, payload };
  }
}

function deserializeEnvelopeForPlayer(
  envelope: string
):
  | {
      playerId: "all" | number;
      payload: string;
    }
  | undefined {
  const parts = deserializeEnvelope(envelope);
  if (!parts) return;
  const { prefix, payload } = parts;
  if (prefix === "all") {
    return { playerId: "all", payload };
  }

  const playerId = +prefix;
  if (!Number.isInteger(playerId)) return;
  return { playerId, payload };
}

export class EnvelopeToServer extends Envelope<"server"> {
  constructor(json: string, readonly message: MessageToServer) {
    super("server", json);
  }

  static deserialize(str: string): EnvelopeToServer | undefined {
    if (!str.startsWith("server:")) return;
    const { payload } = deserializeEnvelope(str)!;

    try {
      const message = JSON.parse(payload);
      if (this.isValidMessage(message)) {
        return new EnvelopeToServer(payload, message);
      } else {
        return;
      }
    } catch {
      return;
    }
  }

  private static isValidMessage(msg: MessageToServer): msg is MessageToServer {
    switch (msg?.type) {
      case "join":
        return (
          is(msg.idOrSlug, "string", "number") &&
          is(msg.password, "string", "undefined")
        );

      case "create":
      case "leave":
        return true;

      case "setMyInfo":
        return (
          is(msg.name, "string", "undefined") &&
          is(msg.publicData, "string", "undefined")
        );

      case "setRoomInfo":
        return (
          Boolean(msg.publicData || msg.publicLog) &&
          is(msg.publicData, "object", "undefined") &&
          (!msg.publicData ||
            (is(msg.publicData.editToken, "string") &&
              is(msg.publicData.payload, "string", "undefined"))) &&
          is(msg.publicLog, "object", "undefined") &&
          (!msg.publicLog ||
            (is(msg.publicLog.editToken, "string", "undefined") &&
              is(msg.publicLog.payload, "string")))
        );

      default: {
        const _dummy: never = msg;
        return false;
      }
    }

    function is<T>(
      val: T,
      ...types: (
        | "string"
        | "number"
        | "bigint"
        | "boolean"
        | "symbol"
        | "undefined"
        | "object"
        | "function"
      )[]
    ) {
      return types.includes(typeof val);
    }
  }
}

export class EnvelopeFromServer extends Envelope<"server"> {
  constructor(payload: MessageFromServer) {
    super("server", JSON.stringify(payload));
  }
}

export class EnvelopeToPlayer extends Envelope<number | "all"> {
  // prettier-ignore
  declare private dummy: string;
  constructor(readonly recipient: number | "all", payload: string) {
    super(recipient, payload);
  }

  static deserialize(envelope: string): EnvelopeToPlayer | undefined {
    const parts = deserializeEnvelopeForPlayer(envelope);
    if (!parts) return;
    return new EnvelopeToPlayer(parts.playerId, parts.payload);
  }
}

export class EnvelopeFromPlayer extends Envelope<number> {
  // prettier-ignore
  declare private dummy: string;
  constructor(readonly sender: number, payload: string) {
    super(sender, payload);
  }
}

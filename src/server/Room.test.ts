import { v4 } from "https://deno.land/std@0.90.0/uuid/mod.ts";
import { _ } from "../../test/infrastructure/should.ts";
import { MessageFromServer } from "../shared/messages.ts";
import {
  EnvelopeFromPlayer,
  EnvelopeFromServer,
  EnvelopeToPlayer,
} from "./Envelope.ts";
import { RoomConnectionCallbacks } from "./Room.ts";
import { RoomDatabase } from "./RoomDatabase.ts";

interface MessageForPlayer {
  type: "msgForPlayer";
  payload: unknown;
}

type AnyMessage = MessageForPlayer | MessageFromServer;

class MessageListener {
  envelopes: (EnvelopeFromPlayer | EnvelopeFromServer)[] = [];
  messages: AnyMessage[] = [];

  callbacks: RoomConnectionCallbacks = {
    receiveMessage: (envelope) => {
      this.envelopes.push(envelope);
      const parsedMessage = JSON.parse(envelope.payload);
      _(parsedMessage).shouldNotBeNullish("Parse message");
      this.messages.push(parsedMessage!);
    },
    kickOut: () => {
      // Not handled
    },
  };

  getMessages<T extends AnyMessage["type"]>(
    sender: "server" | number,
    type: T
  ) {
    const senderAsPrefix = sender.toString();
    return this.messages.filter(
      (x, index): x is AnyMessage & { type: T } =>
        x.type === type && this.envelopes[index].prefix == senderAsPrefix
    );
  }

  getSingleMessage<T extends AnyMessage["type"]>(
    sender: "server" | number,
    type: T
  ) {
    const filtered = this.getMessages(sender, type);
    _(filtered).shouldContainSingle();
    return filtered[0];
  }
}

function setupRoom() {
  const messages = new MessageListener();
  const database = new RoomDatabase();
  const roomConnection = database.createRoom().join({
    callbacks: messages.callbacks,
  });

  const joinInfo = messages.getSingleMessage("server", "joinInfo");

  return {
    database,
    joinInfo,
    roomConnection,
    messages,
  };
}

Deno.test("Room.create", () => {
  const messages = new MessageListener();

  const database = new RoomDatabase();
  const roomConnection = database.createRoom().join({
    callbacks: messages.callbacks,
  });

  _(roomConnection).shouldNotBeNullish();
  _(messages.getSingleMessage("server", "joinInfo")).shouldNotBeNullish();

  roomConnection?.leave();
  database.dispose();
});

Deno.test("Room.join", () => {
  const { joinInfo, database } = setupRoom();

  const messages = new MessageListener();

  const roomConnection = database.getRoom(joinInfo.roomId)?.join({
    callbacks: messages.callbacks,
  });

  _(roomConnection).shouldNotBeNullish();

  const newJoinInfo = messages.getSingleMessage("server", "joinInfo");

  _(newJoinInfo.roomId).shouldBe(joinInfo.roomId);
  _(newJoinInfo.slug).shouldBe(joinInfo.slug);
  _(newJoinInfo.password).shouldNotBe(joinInfo.password);

  roomConnection?.leave();
  database.dispose();
});

Deno.test("Room.join - non-existent", () => {
  const database = new RoomDatabase();
  const roomConnection = database.getRoom(v4.generate());
  _(roomConnection).shouldBeUndefined();

  database.dispose();
});

Deno.test("Room.join - rejoin", () => {
  const { joinInfo, messages, database } = setupRoom();

  const newMessages = new MessageListener();
  const roomConnection = database.getRoom(joinInfo.roomId)?.rejoin({
    password: joinInfo.password,
    callbacks: newMessages.callbacks,
  });

  _(roomConnection).shouldNotBeNullish();

  const kickedOutError = messages
    .getMessages("server", "error")
    .filter((x) => x.reason === "kickedOut");
  _(kickedOutError).shouldContainSingle();

  database.dispose();
});

Deno.test("RoomConnection.sendMessage", () => {
  const { joinInfo: joinInfo0, messages: messages0, database } = setupRoom();

  const messages1 = new MessageListener();
  const roomConnection1 = database.getRoom(joinInfo0.roomId)?.join({
    callbacks: messages1.callbacks,
  })!;
  _(roomConnection1).shouldNotBeNullish();

  const message = {
    type: "msgForPlayer",
    payload: 345,
  } as MessageForPlayer;
  roomConnection1.sendMessage(new EnvelopeToPlayer(0, JSON.stringify(message)));

  const receivedMessage = messages0.getSingleMessage(1, "msgForPlayer");

  _(receivedMessage).shouldBe(message);

  database.dispose();
});

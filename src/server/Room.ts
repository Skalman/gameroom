import { MessageFromServer, MessageToServer } from "../shared/messages.ts";
import {
  EnvelopeFromPlayer,
  EnvelopeFromServer,
  EnvelopeToPlayer,
} from "./Envelope.ts";
import { randomString } from "./random.ts";

class Player {
  readonly playerId: number;
  playerName?: string;
  isActive = true;
  password = randomString();
  publicData?: string;
  private callbacks: RoomConnectionCallbacks;

  constructor(opts: { id: number; callbacks: RoomConnectionCallbacks }) {
    this.playerId = opts.id;
    this.callbacks = opts.callbacks;
  }

  sendMessage(envelope: EnvelopeFromPlayer | EnvelopeFromServer): void {
    if (this.isActive) {
      this.callbacks.receiveMessage(envelope);
    }
  }

  replaceCallbacks(callbacks: RoomConnectionCallbacks) {
    this.callbacks = callbacks;
  }

  toPublic(): MessageFromServer.Player {
    return {
      playerId: this.playerId,
      isActive: this.isActive,
      playerName: this.playerName,
      publicData: this.publicData,
    };
  }

  toPlainObject() {
    return {
      playerId: this.playerId,
      playerName: this.playerName,
      isActive: this.isActive,
      password: this.password,
      publicData: this.publicData,
    };
  }

  static fromPlainObject(
    data: ReturnType<typeof Player["prototype"]["toPlainObject"]>
  ) {
    const player = new Player({
      id: data.playerId,
      // deno-lint-ignore no-explicit-any
      callbacks: undefined as any,
    });

    player.playerName = data.playerName;
    player.isActive = false;
    player.password = data.password;
    player.publicData = data.publicData;

    return player;
  }
}

export interface RoomConnectionCallbacks {
  /** Called when this player receives messages from other players or from the room itself */
  receiveMessage(envelope: EnvelopeFromPlayer | EnvelopeFromServer): void;
  /** Called when this player is kicked out of the room */
  kickOut(): void;
}

export interface RoomConnection {
  /** Send message to other players in the room */
  sendMessage(envelope: EnvelopeToPlayer): void;
  leave(): void;
  setMyInfo(msg: MessageToServer.SetMyInfo): void;
  setRoomInfo(msg: MessageToServer.SetRoomInfo): void;
}

export class Room {
  serialize(): string {
    return JSON.stringify(this.toPlainObject());
  }

  static deserialize(str: string): Room {
    const data = JSON.parse(str);
    return this.fromPlainObject(data);
  }

  private toPlainObject() {
    return {
      roomId: this.roomId,
      slug: this.slug,
      publicData: this.publicData,
      publicLog: this.publicLog,
      activePlayerCount: this.activePlayerCount,
      lastActivity: this.lastActivity,
      players: this.players.map((x) => x.toPlainObject()),
    };
  }

  private static fromPlainObject(
    data: ReturnType<typeof Room["prototype"]["toPlainObject"]>
  ) {
    const room = new Room(data.roomId, data.slug);
    Object.assign(room.publicData, data.publicData);
    Object.assign(room.publicLog, data.publicLog);
    room.lastActivity = data.lastActivity;
    room.players.push(...data.players.map(Player.fromPlainObject));

    return room;
  }

  private changeListeners: (() => void)[] = [];
  addChangeListener(callback: () => void) {
    this.changeListeners.push(callback);
  }
  private notifyChangeListeners() {
    for (const listener of this.changeListeners) {
      listener();
    }
  }

  readonly players: Player[] = [];
  lastActivity = Date.now();
  activePlayerCount = 0;
  readonly publicData: MessageFromServer.PublicData = {
    editToken: randomString(),
  };
  readonly publicLog: MessageFromServer.PublicLog = {
    editToken: randomString(),
    items: [],
  };

  constructor(readonly roomId: number, readonly slug: string) {}

  join({ callbacks }: { callbacks: RoomConnectionCallbacks }): RoomConnection {
    const player = new Player({
      id: this.players.length,
      callbacks,
    });

    this.activePlayerCount++;
    this.players.push(player);
    this.sendJoinMessage(player);
    return this.getRoomConnection(player);
  }

  rejoin({
    password,
    callbacks,
  }: {
    password: string;
    callbacks: RoomConnectionCallbacks;
  }): RoomConnection | undefined {
    const player = this.players.find((x) => x.password === password);

    if (!player) {
      callbacks.receiveMessage(
        new EnvelopeFromServer({ type: "error", reason: "badPassword" })
      );
      callbacks.kickOut();
      return;
    }

    this.sendKickOutMessage(player);
    player.replaceCallbacks(callbacks);
    player.isActive = true;
    this.activePlayerCount++;
    this.sendJoinMessage(player);
    return this.getRoomConnection(player);
  }

  private getRoomConnection(player: Player): RoomConnection {
    return {
      leave: () => this.leave(player),
      sendMessage: (envelope) => this.onMessageFromPlayer(player, envelope),
      setMyInfo: (msg) => {
        player.playerName = msg.name;
        player.publicData = msg.publicData;
        this.sendJoinMessage(player);
        this.sendPlayerInfo(player, "all");
      },
      setRoomInfo: (envelope) => this.onSetRoomInfo(player, envelope),
    };
  }

  private leave(player: Player) {
    this.activePlayerCount--;
    this.sendMessage(player, new EnvelopeFromServer({ type: "bye" }));
    player.isActive = false;
    this.sendPlayerInfo(player, "all");
  }

  private sendJoinMessage(player: Player) {
    this.sendMessage(
      player,
      new EnvelopeFromServer({
        type: "joinInfo",
        roomId: this.roomId,
        slug: this.slug,
        playerId: player.playerId,
        playerName: player.playerName,
        password: player.password,
        publicData: player.publicData,
      })
    );

    this.sendRoomInfo([player]);
    this.sendPlayerInfo(player, "others");
  }

  private sendRoomInfo(recipients: Player[]) {
    const envelope = new EnvelopeFromServer({
      type: "roomInfo",
      players: this.players.map((x) => x.toPublic()),
      publicData: this.publicData,
      publicLog: this.publicLog,
    });

    for (const player of recipients) {
      this.sendMessage(player, envelope);
    }
  }

  private sendPlayerInfo(player: Player, recipients: "all" | "others") {
    const playerInfo = new EnvelopeFromServer({
      type: "roomInfo",
      players: [player.toPublic()],
    });

    for (const p of this.players) {
      if (recipients === "all" || p !== player) {
        p.sendMessage(playerInfo);
      }
    }
  }

  private sendKickOutMessage(player: Player) {
    this.sendMessage(
      player,
      new EnvelopeFromServer({
        type: "error",
        reason: "kickedOut",
        message: "You joined this room from another connection",
      })
    );
  }

  private sendMessage(
    recipient: Player,
    message: EnvelopeFromPlayer | EnvelopeFromServer
  ) {
    this.lastActivity = Date.now();
    this.notifyChangeListeners();
    recipient.sendMessage(message);
  }

  private onMessageFromPlayer(
    sender: Player,
    envelope: EnvelopeToPlayer
  ): void {
    const envelopeFromPlayer = new EnvelopeFromPlayer(
      sender.playerId,
      envelope.payload
    );

    if (envelope.recipient === "all") {
      this.lastActivity = Date.now();
      this.notifyChangeListeners();
      for (const player of this.players) {
        player.sendMessage(envelopeFromPlayer);
      }
      return;
    }

    const recipient = this.players[envelope.recipient];
    if (!recipient) {
      this.sendMessage(
        sender,
        new EnvelopeFromServer({
          type: "error",
          reason: "playerDoesntExist",
          message: `Player: ${
            envelope.recipient
          }. Original message: ${envelope.payload.substr(0, 50)}`,
        })
      );
      return;
    }

    this.sendMessage(recipient, envelopeFromPlayer);
  }

  private onSetRoomInfo(player: Player, envelope: MessageToServer.SetRoomInfo) {
    const error = this.getRoomInfoEditConflict(envelope);
    if (error) {
      this.sendMessage(
        player,
        new EnvelopeFromServer({
          type: "error",
          reason: "editConflict",
          message: `Failed to set public room data, because of an edit conflict for error`,
        })
      );
      this.sendRoomInfo([player]);
      return;
    }

    const { publicData, publicLog } = envelope;

    if (publicData) {
      this.publicData.editToken = randomString();
      this.publicData.payload = publicData.payload;
    }

    if (publicLog) {
      this.publicLog.items.push({
        playerId: player.playerId,
        created: Date.now(),
        payload: publicLog.payload,
      });
    }

    this.sendRoomInfo(this.players);
  }

  private getRoomInfoEditConflict({
    publicData,
    publicLog,
  }: MessageToServer.SetRoomInfo) {
    const publicDataError =
      publicData && this.publicData.editToken !== publicData.editToken;

    const publicLogError =
      publicLog?.editToken && this.publicLog.editToken !== publicLog.editToken;

    return [publicDataError && "publicData", publicLogError && "publicLog"]
      .filter((x) => x)
      .join("; ");
  }
}

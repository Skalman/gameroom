import { DB } from "https://deno.land/x/sqlite@v2.4.0/mod.ts";
import { QueryParam } from "https://deno.land/x/sqlite@v2.4.0/src/db.ts";
import { adjectives } from "../assets/adjectives.js";
import { nouns } from "../assets/nouns.js";
import { random } from "./random.ts";
import { Room } from "./Room.ts";

export class RoomDatabase {
  private readonly activeRooms = new Map<string, Room>();
  private readonly timeoutHandles = new Set<number>();
  private readonly db: DB;

  constructor(filename?: string) {
    this.db = RoomDatabase.createDbConnection(filename);
  }

  dispose() {
    for (const handle of this.timeoutHandles) {
      clearTimeout(handle);
    }

    this.db.close();
  }

  getDebug(idOrSlug: string | undefined) {
    if (!idOrSlug) {
      return [...new Set(this.activeRooms.values())]
        .sort((a, b) => b.lastActivity - a.lastActivity)
        .map((x) => ({
          id: x.roomId,
          slug: x.slug,
          playerCount: x.players.length,
          activePlayerCount: x.activePlayerCount,
          lastActivity: new Date(x.lastActivity),
        }));
    } else {
      const x = this.activeRooms.get(idOrSlug);
      if (!x) return;

      return {
        id: x.roomId,
        slug: x.slug,
        playerCount: x.players.length,
        activePlayerCount: x.activePlayerCount,
        lastActivity: new Date(x.lastActivity),
        players: x.players.map((p) => p.toPublic()),
      };
    }
  }

  private static createDbConnection(filename?: string) {
    const db = new DB(filename);

    db.query(`
      CREATE TABLE IF NOT EXISTS Room (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        publicId TEXT NOT NULL,
        slug TEXT NOT NULL,
        data TEXT NOT NULL,
        created INTEGER NOT NULL,
        updated INTEGER NOT NULL
      );

      CREATE UNIQUE INDEX IF NOT EXISTS Room_publicId
      ON Room(publicId);

      CREATE UNIQUE INDEX IF NOT EXISTS Room_slug
      ON Room(slug);
    `);

    return db;
  }

  private queryFirst(
    sql: string,
    values?: Record<string, QueryParam> | QueryParam[] | undefined
    // deno-lint-ignore no-explicit-any
  ): Record<string, any> | undefined {
    const [result] = this.db.query(sql, values).asObjects();
    return result;
  }

  private getPublicRoomIdExists(publicId: string) {
    return !!this.queryFirst("SELECT 0 FROM Room WHERE publicId = :publicId", {
      publicId,
    });
  }

  private getSlugExists(slug: string) {
    return !!this.queryFirst("SELECT 0 FROM Room WHERE slug = :slug", {
      slug,
    });
  }

  private getNewPublicId() {
    let id = (100 + random(900)).toString();
    while (this.getPublicRoomIdExists(id)) {
      id += random(10);
    }
    return id;
  }

  private getNewSlug() {
    let slug = nouns[random(nouns.length)];
    do {
      slug = adjectives[random(adjectives.length)] + "-" + slug;
    } while (this.getSlugExists(slug));
    return slug;
  }

  createRoom(): Room {
    const publicId = this.getNewPublicId();
    const slug = this.getNewSlug();

    const room = new Room(+publicId, slug);
    this.addRoomChangeListener(room);

    this.db.query(
      `
        INSERT INTO Room (publicId, slug, data, created, updated)
        VALUES (:publicId, :slug, :data, :created, :updated);
      `,
      {
        publicId,
        slug,
        data: room.serialize(),
        created: Date.now(),
        updated: Date.now(),
      }
    );

    this.activeRooms.set(publicId, room);
    this.activeRooms.set(slug, room);

    return room;
  }

  getRoom(publicIdOrSlug: string | number): Room | undefined {
    const activeRoom = this.activeRooms.get(publicIdOrSlug + "");

    if (activeRoom) {
      return activeRoom;
    }

    const roomData = this.queryFirst(
      `
        SELECT data
        FROM Room
        WHERE
          publicId = :publicIdOrSlug
          OR slug = :publicIdOrSlug;
      `,
      { publicIdOrSlug }
    );

    if (!roomData) {
      return;
    }

    const dbRoom = Room.deserialize(roomData.data);
    this.addRoomChangeListener(dbRoom);

    this.activeRooms.set(dbRoom.roomId + "", dbRoom);
    this.activeRooms.set(dbRoom.slug, dbRoom);

    return dbRoom;
  }

  private addRoomChangeListener(room: Room) {
    let timeoutHandle: number | undefined;

    room.addChangeListener(() => {
      if (timeoutHandle) {
        this.timeoutHandles.delete(timeoutHandle);
        clearTimeout(timeoutHandle);
      }

      timeoutHandle = setTimeout(() => {
        this.db.query(
          `
            UPDATE Room
            SET data = :data, updated = :updated
            WHERE publicId = :publicId;
          `,
          {
            publicId: room.roomId,
            data: room.serialize(),
            updated: Date.now(),
          }
        );

        if (room.activePlayerCount === 0) {
          this.activeRooms.delete(room.roomId + "");
          this.activeRooms.delete(room.slug);
        }
      }, 30_000);

      this.timeoutHandles.add(timeoutHandle);
    });
  }
}

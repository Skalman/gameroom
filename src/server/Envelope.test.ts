import { _ } from "../../test/infrastructure/should.ts";
import { MessageToServer } from "../shared/messages.ts";
import { EnvelopeToServer } from "./Envelope.ts";

const validMessagesToServer: MessageToServer[] = [
  { type: "create" },
  { type: "join", idOrSlug: 123 },
  { type: "join", idOrSlug: "slug" },
  { type: "join", idOrSlug: 123, password: "pwd" },
];
for (const message of validMessagesToServer) {
  const serialized = `server:${JSON.stringify(message)}`;
  Deno.test(`EnvelopeToServer.deserialize - ${serialized}`, () => {
    const envelope = EnvelopeToServer.deserialize(serialized);
    _(envelope).shouldNotBeNullish();
    _(envelope!.message).shouldBe(message);
  });
}

const invalidMessagesToServer = [
  'bad-recipient:{"type":"a"}',
  "server:{}",
  "server:not-json",
  "server:null",
  'server:{"type":"join","idOrSlug":{}}',
];

for (const serialized of invalidMessagesToServer) {
  Deno.test(`EnvelopeToServer.deserialize - invalid - ${serialized}`, () => {
    const envelope = EnvelopeToServer.deserialize(serialized);
    _(envelope).shouldBeUndefined();
  });
}
